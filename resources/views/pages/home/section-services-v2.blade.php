<!-- Services Section Start -->
<div id="rs-services" class="rs-services style1 modify pt-92 pb-84 md-pt-72 md-pb-64">
    <div class="container">
        <div class="sec-title text-center mb-47 md-mb-42">
            <div class="sub-title primary">Services</div>
            <h2 class="title mb-0">Our Best Services</h2>
        </div>
        <div class="row gutter-16">
            <div class="col-lg-3 col-sm-6 mb-16">
                <div class="service-wrap h-100">
                    <div class="icon-part">
                        <img src="assets/images/services/icons/style4/2.png" alt="">
                    </div>
                    <div class="content-part">
                        <h5 class="title"><a href="/reference/services-single">Software Development</a></h5>
                        <div class="desc">We provide software development service including custom-built e-commerce site, CRM and CMS</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-16">
                <div class="service-wrap h-100">
                    <div class="icon-part">
                        <img src="assets/images/services/icons/style10/3-blue.png" alt="">
                    </div>
                    <div class="content-part">
                        <h5 class="title"><a href="/reference/services-single">Digitalize Consultation</a></h5>
                        <div class="desc">Keeping up with digital globalisation, we provide consultation on how to transform your business operation into a more efficient, digitalized practice</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-16">
                <div class="service-wrap h-100">
                    <div class="icon-part">
                        <img src="assets/images/services/icons/style4/4.png" alt="">
                    </div>
                    <div class="content-part">
                        <h5 class="title"><a href="/reference/services-single">Workstation Management</a></h5>
                        <div class="desc">We offer management to set up your networked office which ranges from setup to maintenance of infrastructures, networking, licensing management and more.</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-16">
                <div class="service-wrap h-100">
                    <div class="icon-part">
                        <img src="assets/images/services/icons/modify/6.png" alt="">
                    </div>
                    <div class="content-part">
                        <h5 class="title"><a href="/reference/services-single">CyberSecurity Consultation</a></h5>
                        <div class="desc">Protect your data and identity from being breached. We provide consultation and personnel training to implement best-practice SOP to secure your digital assets.</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row gutter-16 justify-content-center">
            <div class="col-lg-3 col-sm-6 mb-16">
                <div class="service-wrap h-100">
                    <div class="icon-part">
                        <img src="assets/images/services/icons/modify/7.png" alt="">
                    </div>
                    <div class="content-part">
                        <h5 class="title"><a href="/reference/services-single">Cloud Computing</a></h5>
                        <div class="desc">Migrating your assets to the cloud? We can help to facilitate this process with our highly experienced and certified inhouse cloud architects.</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-16">
                <div class="service-wrap h-100">
                    <div class="icon-part">
                        <img src="assets/images/services/icons/modify/4.png" alt="">
                    </div>
                    <div class="content-part">
                        <h5 class="title"><a href="/reference/services-single">Backup & Recovery</a></h5>
                        <div class="desc">We offer disaster recovery and mitigation planning as well as coordination support so you can rest knowing your data is safe.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Services Section End -->