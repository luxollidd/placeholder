
<!-- Banner Section Start -->
<div class="rs-banner style2" style="background: url(assets/images/banner/free-banner-3.jpg);">
    <div class="container relative">
        <div class="row">
            <div class="col-lg-7">
                <div class="content-part">
                    <h1 class="title mb-19 white-color">No. 1 Leader in PMS Solutions</h1>
                    <p class="desc mb-36 white-color">we are dedicated to bring you the best IT solutions in discovering how you can improve on your business efficiency with our expertise.</p>
                    <div class="btn-part">
                        <a class="readon" href="contact.html">Discover More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="bnr-image-wrap">
            <div class="bnr-image">
                <img class="main-img" src="assets/images/index/banner/1-11.png" alt="" data-aos="fade-left" data-aos-duration="1500">
                <img class="fly ly1" src="assets/images/index/banner/2-11.png" alt="" data-aos="fade-up" data-aos-duration="1200" data-aos-delay="1500">
                <img class="fly ly2" src="assets/images/index/banner/3-11.png" alt="" data-aos="fade-up" data-aos-duration="1200" data-aos-delay="600">
                <img class="fly ly3" src="assets/images/index/banner/4-11.png" alt="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1000">
                <img class="fly ly4" src="assets/images/index/banner/5-11.png" alt="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1900">
            </div>
        </div>
    </div>
</div>
<!-- Banner Section End -->