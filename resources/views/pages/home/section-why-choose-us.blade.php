
   
<!-- Services Section Start -->
<div class="rs-services style4 pb-100 pt-100 md-pt-80 md-pb-70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="sec-title mb-100 text-center">
                    <h1 class="title mb-20">Why Choose Us?</h1>
                </div>
            </div>
        </div>
        <div class="row y-middle md-col-padding">
            <div class="col-lg-6 pr-32">
                <div class="image-part md-mt-0 md-mb-30">
                    <img src="/assets/images/index/why-choose-us/left-image.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6 pl-55">
                <div class="row y-middle">
                    <div class="col-lg-12">
                        <div class="sec-title mb-24">
                            <h2 class="title mb-20">Cloud Computed Solutions</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 sm-mb-45 pr-30">
                        <div class="service-wrap mb-45">
                            <div class="icon-part">
                                <img src="assets/images/services/icons/style4/1.png" alt="">
                            </div>
                            <div class="content-part">
                                <h5 class="title"><a href="/reference/services-single">Reliability Guaranteed by AWS</a></h5>
                                <div class="desc">We leverage the quality assured by AWS by running our products and services over AWS-managed layers</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 sm-mb-45 pl-30">
                        <div class="service-wrap">
                            <div class="icon-part">
                                <img src="assets/images/services/icons/style4/3.png" alt="">
                            </div>
                            <div class="content-part">
                                <h5 class="title"><a href="/reference/services-single">No Configuration Needed</a></h5>
                                <div class="desc">Abstracting the “server” part from you so you don’t have to fuss yourselves over the intricacies of setting up the infrastructure</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pr-30">
                        <div class="service-wrap mb-45">
                            <div class="icon-part">
                                <img src="assets/images/services/icons/style4/2.png" alt="">
                            </div>
                            <div class="content-part">
                                <h5 class="title"><a href="/reference/services-single">99.9% server uptime guaranteed </a></h5>
                                <div class="desc">As stipulated by AWS SLA policy <a href="https://aws.amazon.com/compute/sla/">here</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 sm-mb-45 pl-30">
                        <div class="service-wrap">
                            <div class="icon-part">
                                <img src="assets/images/services/icons/style4/4.png" alt="">
                            </div>
                            <div class="content-part">
                                <h5 class="title"><a href="/reference/services-single">Business Analysis</a></h5>
                                <div class="desc">Duis eleifend molestie leo at mollis sanctus intro</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Services Section End -->
<!-- Solutions Section Start -->
<div class="rs-solutions pb-100 md-pb-80" style="background: aliceblue">
    <div class="container">
        <div class="row y-middle">
            <div class="col-lg-6">
                <div class="sec-title mb-24">
                    <h2 class="title mb-20">End-to-End Digitalized Solution</h2>
                    <div class="desc">Reobiz donec pulvinar magna id leoersi pellentesque impered dignissim <br> rhoncus euismod euismod eros vitae.</div>
                </div>
                <ul class="listing-style regular">
                    <li>Single point of contact</li>
                    <li>Fast and effective</li>
                    <li>Our support team is available 24/7 around the clock for your business needs</li>
                </ul>
            </div>
            <div class="col-lg-6 md-order-first md-mb-30 mt-30">
                <div class="image-part">
                    <img src="/assets/images/index/why-choose-us/right-image.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="rs-solutions mt-4 pb-100 md-pb-80">
    <div class="container">
        <div class="row y-middle">
            <div class="col-lg-6 md-order-first md-mb-30">
                <div class="image-part">
                    <img src="/assets/images/index/why-choose-us/left-2-image.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="sec-title mb-24">
                    <h2 class="title mb-20">Expertise on Industry-Level Application Development</h2>
                    <div class="desc">Reobiz donec pulvinar magna id leoersi pellentesque impered dignissim <br> rhoncus euismod euismod eros vitae.</div>
                </div>
                <ul class="listing-style regular">
                    <li>Custom built E-commerce Solution</li>
                    <li>Proven track record for our PMS Solution</li>
                    <li>Trusted by more than 15 Pawnshops here in Malaysia</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Solutions Section End -->