
<!-- Contact Section Start -->
<div class="rs-contact style2 gray-bg">
    <div class="row md-col-padding">
        <div class="col-lg-6 pr-0">
            <div class="g-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d4737.730870499101!2d101.58448176700739!3d3.1103379038673133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2smy!4v1670311888703!5m2!1sen!2smy" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
        <div class="col-lg-6 pl-0">
            <div class="form-part pl-43 md-pl-15 md-pr-15 pt-110 pb-110 md-pt-72 md-pb-80">
                <div class="row">
                    <div class="col-lg-11">
                        <div class="sec-title mb-35">
                            <div class="sub-title primary">CONTACT US</div>
                            <h2 class="title mb-0 white-color">Get In Touch</h2>
                        </div>
                        <div id="form-messages" class="white-color"></div>
                        <form id="contact-form" class="contact-form" method="post" action="mailer.php">
                            <div class="row">
                                <div class="col-md-6 mb-30">
                                    <div class="common-control">
                                        <input type="text" name="name" placeholder="Name" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-30">
                                    <div class="common-control">
                                        <input type="email" name="email" placeholder="Email" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-30">
                                    <div class="common-control">
                                        <input type="text" name="phone" placeholder="Phone Number" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-30">
                                    <div class="common-control">
                                        <input type="text" name="website" placeholder="Your Website" required="">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-30">
                                    <div class="common-control">
                                        <textarea name="message" placeholder="Your Message Here" required=""></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="submit-btn">
                                        <button type="submit" class="readon">Submit Now</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact Section End -->