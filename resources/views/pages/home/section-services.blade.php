
<!-- Premium Services Section Start -->
<div id="rs-services" class="rs-services style2 gray-bg2 pt-100 pb-100 md-pt-80 md-pb-80">
    <div class="container">
        <div class="sec-title style2 mb-60 md-mb-50 sm-mb-42">
            <div class="first-half text-right">
                <div class="sub-title primary">Premium Services</div>
                <h2 class="title mb-0">Services</h2>
            </div>
            <div class="last-half">
                <p class="desc mb-0 pr-30">We are dedicated to offer you only the best service by our professionals. - need more elaboration here -</p>
            </div>
        </div>
        <div class="row gutter-20">
            <div class="col-md-4 sm-mb-30">
                <div class="service-wrap">
                    <div class="image-part">
                        <img src="/assets/images/index/services/software.jpg" alt="" style="height: 262px;">
                    </div>
                    <div class="content-part text-center">
                        <h3 class="title"><a href="/assets/images/index/services/software.jpg">Software Development</a></h3>
                        <div class="desc">We are dedicated in delivering the latest , up-to-date technology used in modern softwares. Alphacloud provides you with the highest quality software development services such as custom-built E-Commerce website, WMS, CRM, CMS, and more. All personally tailored to your business needs.</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sm-mb-30">
                <div class="service-wrap">
                    <div class="image-part">
                        <img src="/assets/images/index/services/digitalization-2.jpg" alt="" style="height: 262px;">
                    </div>
                    <div class="content-part text-center">
                        <h3 class="title"><a href="/assets/images/index/services/digitalization.jpg">Digitalize Consultation</a></h3>
                        <div class="desc">With our experiences in dealing with clients from many different backgrounds and needs, we can offer you business consultation as a service in which we can identify and find what your business is lacking, and how to turn these challenges into opportunities in IT context.</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-wrap">
                    <div class="image-part">
                        <img src="/assets/images/index/services/workstation.jpg" alt="" style="height: 262px;">
                    </div>
                    <div class="content-part text-center">
                        <h3 class="title"><a href="/assets/images/index/services/workstation.jpg">Workstation Management</a></h3>
                        <div class="desc">Further enhance your workspace productivity by introducing networking-technology in it. We specialize in handling workstation management such as local infrastructure set up, workstation installation, licensing management, and more</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Premium Services Section End -->