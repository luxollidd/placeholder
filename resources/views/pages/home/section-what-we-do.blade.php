
    <!-- Services Section Start -->
    <div class="rs-services style6 bg12 bg-fixed pt-98 pb-92 md-pt-72 md-pb-72" style="background-image:url(assets/images/bg/what-we-do-1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 md-mb-40">
                    <div class="sec-title">
                        <div class="sub-title white-color">What we are about</div>
                        <h2 class="title3 mb-35 white-color">Delivering the best IT Solutions to match your business needs!</h2>
                        <div class="btn-part">
                            <a class="readon transparent white" href="services-single.html">View Services</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-sm-6 sx-mb-40">
                            <div class="services-wrap mb-40">
                                <div class="icon-part">
                                    <i class="fa fa-bullhorn"></i>
                                </div>
                                <h4 class="title"><a href="/reference/services-single">Profit Planning</a></h4>
                                <div class="desc">At vero eos et accusamus etius too odio dignissimos</div>
                            </div>
                            <div class="services-wrap">
                                <div class="icon-part">
                                    <i class="fa fa-file-o"></i>
                                </div>
                                <h4 class="title"><a href="/reference/services-single">Reports Analysis</a></h4>
                                <div class="desc">At vero eos et accusamus etius too odio dignissimos</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="services-wrap mb-40">
                                <div class="icon-part">
                                    <i class="fa fa-suitcase"></i>
                                </div>
                                <h4 class="title"><a href="/reference/services-single">Business Analysis</a></h4>
                                <div class="desc">At vero eos et accusamus etius too odio dignissimos</div>
                            </div>
                            <div class="services-wrap">
                                <div class="icon-part">
                                    <i class="fa fa-file-powerpoint-o"></i>
                                </div>
                                <h4 class="title"><a href="/reference/services-single">Project Reporting</a></h4>
                                <div class="desc">At vero eos et accusamus etius too odio dignissimos</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Services Section End -->