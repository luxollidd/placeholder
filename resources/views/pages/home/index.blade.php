@extends('layout.master')
@section('header') @include('component.header-transparent') @endsection
@section('footer') @include('component.footer') @endsection
@section('content')
<!-- Main content Start -->
<div class="main-content">
    @include('pages.home.section-banner')           <!-- Banner -->
    @include('pages.home.section-why-choose-us')    <!-- Why Choose Us -->
    @include('pages.home.section-what-we-do')       <!-- What We Do -->
    @include('pages.home.section-services-v2')      <!-- Services -->
    @include('pages.home.section-portfolio')        <!-- Portfolio -->
    @include('pages.home.section-form')             <!-- Contact Us Form -->
    @include('pages.home.section-form-v2')          <!-- Contact Us Form 2 -->
    @include('pages.home.section-form-v3')          <!-- Contact Us Form 3 -->
    @include('pages.home.section-contact-us')       <!-- Contact Us -->
</div> 
<!-- Main content End -->
@endsection