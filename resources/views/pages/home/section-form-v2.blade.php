<!-- Free Quote Section Start -->
<div class="rs-quote style1 pt-92 pb-100 md-pt-72 md-pb-80">
    <div class="container">
        <div class="sec-title style2 mb-92 md-mb-42">
            <div class="first-half">
                <div class="sub-title gray-color">Free Quote</div>
                <h2 class="title mb-0">Get a free quote to create your desired insurance.</h2>
            </div>
            <div class="last-half">
                <p class="desc mb-0 pr-10">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old working with clients with better services.</p>
            </div>
        </div>
    </div>
    <div class="row md-row-container">
        <div class="col-lg-6 pr-40 md-pr-15 md-mb-30">
            <div class="image-part text-right">
                <img src="assets/images/quote/quote.jpg" alt="">
            </div>
        </div>
        <div class="col-lg-6 custom">
            <div id="form-messages"></div>
            <form id="contact-form" class="quote-form" method="post" action="mailer.php" style="background: rgba(15, 109, 211, 0.8705882352941177)">
                <div class="form-ctrl mb-20">
                    <label>Enter Your Name</label>
                    <input class="index-form-input" type="text" name="name" placeholder="Enter Your Name" required>
                </div>
                <div class="form-ctrl mb-20">
                    <label>Enter Your Email</label>
                    <input class="index-form-input" type="email" name="email" placeholder="Enter Your Email" required>
                </div>
                <div class="form-ctrl mb-20">
                    <label>Type of Enquiry</label>
                    <span class="select-option">
                        <select required class="index-form-input">
                            <option value="">---</option>
                            <option value="Product">Product</option>
                            <option value="Services">Services</option>
                            <option value="Customer Support">Customer Support</option>
                            <option value="Open">Open</option>
                        </select>
                    </span>
                </div>
                <div class="form-ctrl mb-20">
                    <label>Your Employment</label>
                    <input class="index-form-input" type="text" name="employment" placeholder="Your Employment" required>
                </div>
                <div class="form-ctrl mb-30">
                    <label>Annual Income</label>
                    <input class="index-form-input" type="text" name="income" placeholder="Annual Income" required>
                </div>
                <div class="submit-btn index-form-input">
                    <button type="submit" class="title-color">Get Quote Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Free Quote Section End -->