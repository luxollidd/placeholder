<!-- Portfolio Section Start -->
<div class="rs-portfolio style2 pt-100 pb-70 md-pt-80 md-pb-50">
    <div class="row custom-width">
        <div class="col-xl-3 offset-lg relative">
            <div class="title-wrap">
                <div class="title-part">
                    <h2 class="title">Portfolio</h2>
                    <span class="watermark">Port</span>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-12">
            <div class="slider-part">
                <div class="rs-carousel owl-carousel dot-style1" data-loop="true" data-items="3" data-margin="20" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="3" data-lg-device="3" data-md-device-nav="false" data-md-device-dots="false">
                    <div class="portfolio-wrap">
                        <div class="img-part">
                            <a href="/reference/standard"><img src="/assets/images/index/portfolio/shop-1.jpg" alt=""></a>
                        </div>
                        <div class="content-part">
                            <h3 class="title"><a href="/reference/standard">Pawnzon</a></h3>
                            <span class="categories"><a href="/reference/standard">Pawnshop</a></span>
                        </div>
                    </div>
                    <div class="portfolio-wrap">
                        <div class="img-part">
                            <a href="/reference/standard"><img src="/assets/images/index/portfolio/shop-2.jpg" alt=""></a>
                        </div>
                        <div class="content-part">
                            <h3 class="title"><a href="/reference/standard">Kedai Emas Gemilang (KEG)</a></h3>
                            <span class="categories"><a href="/reference/standard">E-Commerce</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Portfolio Section End -->