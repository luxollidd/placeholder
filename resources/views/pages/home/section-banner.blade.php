<!-- Slider Start -->
<div id="rs-slider" class="rs-slider slider6">
    <div class="slider-carousel owl-carousel">
        <!-- Slide 1 -->
        <div class="slider slide1" style="background: url(assets/images/banner/free-banner-3.jpg);">
            <div class="container">
                <div class="content-part">
                    <div class="slider-des">
                        <div class="sl-subtitle mb-16">Made by Professionals</div>
                        <h1 class="sl-title mb-18">No. 1 Leader in PMS Solutions</h1>
                        <div class="sl-desc">we are dedicated to bring you the best IT solutions in discovering how you can improve on your business efficiency with our expertise</div>
                    </div>
                    <div class="slider-bottom mt-40">
                        <ul>
                            <li><a href="/reference/contact" class="readon sl style6">About Us</a></li>
                            <li><a href="/reference/contact" class="readon sl style6 active">What is PMS</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slide 2 -->
        <div class="slider slide1" style="background: url(assets/images/banner/free-banner-2.jpg);">
            <div class="container text-center d-flex">
                <div class="content-part mx-auto">
                    <div class="slider-des mb-5">
                        <h1 class="sl-title">Best Quality, Guaranteed</h1>
                        <div class="sl-desc">We deliver products and services of best of qualities equipped with the latest tech-stack.</div>
                    </div>
                    <div class="slider-bottom">
                        <ul>
                            <li><a href="/reference/contact" class="readon sl style6">Learn More</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Slider End -->