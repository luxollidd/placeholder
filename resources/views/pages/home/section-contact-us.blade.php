

<!-- Contact Section Start -->
<div id="rs-contact" class="rs-contact inner pt-100 md-pt-80">
    <div class="container">
        <div class="content-info-part mb-60">
            <div class="row gutter-16">
                <div class="col-lg-4 md-mb-30">
                    <div class="info-item" style="height: 100%">
                        <div class="icon-part">
                            <i class="fa fa-at"></i>
                        </div>
                        <div class="content-part">
                            <h4 class="title">Phone Number</h4>
                            <a href="tel:+088589-8745">(+088)589-8745</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 md-mb-30">
                    <div class="info-item" style="height: 100%">
                        <div class="icon-part">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="content-part">
                            <h4 class="title">Email Address</h4>
                            <a href="mailto:customersupport@alphacloud.com.my">customersupport@alphacloud.com.my</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="info-item" style="height: 100%">
                        <div class="icon-part">
                            <i class="fa fa-map-o"></i>
                        </div>
                        <div class="content-part">
                            <h4 class="title">Office Address</h4>
                            <p>B-03-6, Pacific Place Commercial Center, Jalan PJU 1a/4, Ara Damansara, Petaling Jaya, Selangor</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact Section End -->

<!-- Map Section Start -->
<div id="rs-contact" class="rs-contact style1 inner">
    <div class="g-map modify">
        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d248.99638796257983!2d101.585761!3d3.1099933!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2smy!4v1669952106977!5m2!1sen!2smy" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
</div>
<!-- Map Section End -->