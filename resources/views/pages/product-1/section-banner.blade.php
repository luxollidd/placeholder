
<!-- Main content Start -->
<div class="main-content">
    <!-- Breadcrumbs Section Start -->
    <div class="rs-breadcrumbs bg-3">
        <div class="container">
            <div class="content-part text-center pt-160 pb-160">
                <h1 class="breadcrumbs-title white-color mb-0">ACHUB</h1>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs Section End -->

    <!-- Services Section Start -->
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 pr-40 md-pr-15 md-mb-50">
                    <img class="mb-39" src="assets/images/services/single/service-single.jpg" alt="">

                    <h2>The #1 PMS Solution in The Market</h2>

                    <p class="desc mb-31">Pawn Management System (PMS) is a full digitalised solution for Pawn Busineses. As an IT Solutions and Service Providers, we come up with an integrated pawn system designed to manage and track the expansion of your pawn business by streamlining the operational procedures. While fulfilling the technology requirements of the gold loan finance, jewellery finance, pawn shop, and pawn broker industries, it also potentially to retain your customer and motivate them to pawn more with you. As it has been specifically crafted to satisfy both pawn customer and pawn businesses. All of these integrated accounting features make it easier for the pawn broker to handle their accounts. The Malaysian government's rules and regulations are followed by the pawnbroker system.</p>
                    
                    <ul class="listing-style regular2 pl-10 sm-pl-0">
                        <li>Fully Digitalised Solution</li>
                        <li>Centralised and Multi-Branch Integration</li>
                        <li>Report and Analysis for Projections</li>
                    </ul>

                    <div class="rs-skillbar style3 pt-52 pb-55">
                        <div class="row gutter-20">
                            <div class="col-md-4 sm-mb-30">
                                <div class="content-part">
                                    <div class="rs-pie-content">
                                        <div class="rs-pie2" data-percent="89">
                                            <span class="rspie-value"></span>
                                        </div>
                                    </div>
                                    <div class="pie-title">Strategy</div>
                                </div>
                            </div>
                            <div class="col-md-4 sm-mb-30">
                                <div class="content-part">
                                    <div class="rs-pie-content">
                                        <div class="rs-pie2" data-percent="82">
                                            <span class="rspie-value"></span>
                                        </div>
                                    </div>
                                    <div class="pie-title">Analytics</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="content-part">
                                    <div class="rs-pie-content">
                                        <div class="rs-pie2" data-percent="95">
                                            <span class="rspie-value"></span>
                                        </div>
                                    </div>
                                    <div class="pie-title">Result</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="desc mb-53">ACHub is one of the solution that we provide for PMS. Customer billing, inventory tracking, cashier control, government reporting, front end & back end reporting, late letters, and SMS are all provided and can be controlled in a single application. The amount of human error is reduced by automating business operations. ACHub let pawnbroker to further manage their process from the point customer walk-in to the store until they redeem or the item being auctioned.</p>
                    <p class="desc mb-53">Suitable for pawnbroking companies, especially when having several branches under the same group.</p>
                    <div class="row gutter-20">
                        <div class="col-6">
                            <img class="bdru-4" src="assets/images/services/single/1.jpg" alt="">
                        </div>
                        <div class="col-6">
                            <img class="bdru-4" src="assets/images/services/single/2.jpg" alt="">
                        </div>
                    </div>

                    <div class="rs-testimonial style6 mt-60">
                        <div class="testi-wrap">
                            <div class="icon-part">
                                <img src="assets/images/testimonial/sign.png" alt="">
                            </div>
                            <p class="desc">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos et quas molestias data.</p>
                            <div class="detail-part">
                                <div class="avatar">
                                    <img src="assets/images/testimonial/avatar/5.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="name">John Doe</div>
                                    <span class="designation">Designer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <ul class="page-nav-vertical mb-50">
                        <li class="active"><a href="#">Detailed Dashboard for Data Overview</a></li>
                        <li><a href="#">Comprehensive Ticketing System</a></li>
                        <li><a href="#">Extensive and Detailed Reporting System</a></li>
                        <li><a href="#">Centralised Datas from Multiple Branch</a></li>
                        <li><a href="#">User-Access Control Integration</a></li>
                    </ul>

                    <div class="addd mb-50">
                        <div class="contact-icon"> <i class="fa fa-phone"></i></div>
                        <h2 class="title white-color">Have any Questions? <br> Call us Today!</h2>
                        <div class="contact white">
                            <a href="tel:123222-8888">(123) 222-8888</a>
                        </div>
                    </div>

                    <div class="brochures">
                        <h3 class="title mb-20">Brochures</h3>
                        <p class="desc mb-30">Cras enim urna, interdum nec por ttitor vitae, sollicitudin eu erosen. Praesent eget mollis nulla sollicitudin.</p>
                        <div class="dual-btn modify">
                            <div class="dual-btn-wrap">
                                <a class="btn-left" href="#"><span>Download</span></a>
                                <span class="connector"> Or </span>
                            </div>
                            <div class="dual-btn-wrap">
                                <a class="btn-right" href="#"><span>Discover</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Services Section End -->
</div> 
<!-- Main content End -->