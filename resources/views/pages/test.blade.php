@extends('layout.master')
@section('header')
    @include('component.header-transparent')
@endsection
@section('content')
<!-- Main content Start -->
<div class="main-content">
    <!-- Banner Section Start -->
    <div class="rs-banner style2">
        <div class="container relative">
            <div class="row">
                <div class="col-lg-12">
                    <div class="content-part">
                        <h1 class="title mb-19 white-color">MAOMAO AWAVU</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Section End -->
</div> 
<!-- Main content End -->
@endsection