<!--Full width header Start-->
<div class="full-width-header header-style2 red modify">
    <!--Header Start-->
    <header id="rs-header" class="rs-header">
        <!-- Menu Start -->
        <div class="menu-area menu-sticky">
            <div class="container-fluid2">
                <div class="row y-middle">
                    <div class="col-lg-2">
                        <div class="logo-area">
                            <a class="dark" href="/"><img src="assets/images/logo-alphacloud.png" alt="logo"></a>
                            <a class="light" href="/"><img src="assets/images/logo-alphacloud.png" alt="logo"></a>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="rs-menu-area">
                            <div class="main-menu">
                                <div class="mobile-menu">
                                    <a class="rs-menu-toggle">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                </div>
                                <nav class="rs-menu">
                                    <ul class="nav-menu">
                                        @include('component.header-content')
                                    </ul> <!-- //.nav-menu -->
                                </nav>
                            </div> <!-- //.main-menu -->
                            <div class="expand-btn-inner">
                                <div class="head-cta">
                                    <i class="flaticon-call"></i>
                                    <a href="tel:+60377341836">(+603)7734-1836</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Menu End -->
    </header>
    <!--Header End-->
</div>
<!--Full width header End-->