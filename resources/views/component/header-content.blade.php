
<li class="menu-item-has-children">
    <a href="#">Pages</a>
    <ul class="sub-menu">
        <li><a href="/">Home V1</a></li>
        <li><a href="/v2">Home V2</a></li>
    </ul>
</li>
<li class="menu-item-has-children">
    <a href="#">Products</a>
    <ul class="sub-menu">
        <li><a href="/product-1">ACHub</a></li>
        <li><a href="#">Pawnlink</a></li>
    </ul>
</li>
<li class="menu-item-has-children">
    <a href="#">Services</a>
    <ul class="sub-menu">
        <li><a href="#">Home V1</a></li>
        <li><a href="#">Home V2</a></li>
    </ul>
</li>
<li class="menu-item-has-children">
    <a href="#">Portfolios</a>
    <ul class="sub-menu">
        <li><a href="#">Pawnzon</a></li>
        <li><a href="#">KEG</a></li>
    </ul>
</li>