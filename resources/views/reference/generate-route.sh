#!/bin/sh

cd /var/www/reobiz-ac/resources/views/reference

for FILE in *;
do
    str=$FILE;
    echo "Route::get('/${str%.*.*}', function () {return view('reference.${str%.*.*}');});" >> test.txt 
done