<!DOCTYPE html>
<html lang="zxx">
    <head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>Contact 2 – Reobiz – Consulting Business HTML Template</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/fav.png">
        <!-- Bootstrap v4.4.1 css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
        <!-- aos css -->
        <link rel="stylesheet" type="text/css" href="assets/css/aos.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href="assets/css/off-canvas.css">
        <!-- linea-font css -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/linea-fonts.css">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/flaticon.css">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">
        <!-- Main Menu css -->
        <link rel="stylesheet" href="assets/css/rsmenu-main.css">
        <!-- nivo slider CSS -->
        <link rel="stylesheet" type="text/css" href="assets/inc/custom-slider/css/nivo-slider.css">
        <link rel="stylesheet" type="text/css" href="assets/inc/custom-slider/css/preview.css">
        <!-- rsmenu transitions css -->
        <link rel="stylesheet" href="assets/css/rsmenu-transitions.css">
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href="assets/css/rs-spacing.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="style.css"> <!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    </head>
    <body>

        <!-- Preloader area start here -->
        <div id="loader" class="loader">
            <div class="spinner"></div>
        </div>
        <!--End preloader here -->

        <!--Full width header Start-->
        <div class="full-width-header">
            <!-- Toolbar Start -->
            <div class="toolbar-area hidden-md">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="toolbar-contact">
                                <ul>
                                    <li><i class="flaticon-email"></i><a href="mailto:info@yourwebsite.com">support@rstheme.com</a></li>
                                    <li><i class="flaticon-call"></i><a href="tel:+123456789">(+123) 456789</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="toolbar-sl-share">
                                <ul>
                                    <li class="opening"> <i class="flaticon-clock"></i> Mon - Fri: 9:00 am - 06.00pm / Closed on Weekends</li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Toolbar End -->
            
            <!--Header Start-->
            <header id="rs-header" class="rs-header">
                <!-- Menu Start -->
                <div class="menu-area menu-sticky">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="logo-area">
                                    <a href="/reference/index"><img src="assets/images/logo-dark.png" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-9 text-right">
                                <div class="rs-menu-area">
                                    <div class="main-menu">
                                        <div class="mobile-menu">
                                            <a class="rs-menu-toggle">
                                                <i class="fa fa-bars"></i>
                                            </a>
                                        </div>
                                        <nav class="rs-menu pr-65">
                                            <ul class="nav-menu">
                                                <li class="rs-mega-menu mega-rs menu-item-has-children"> <a href="#">Home</a>
                                                    <ul class="mega-menu"> 
                                                        <li class="mega-menu-container">
                                                            <div class="mega-menu-innner">
                                                                <div class="single-megamenu">
                                                                    <ul class="sub-menu">
                                                                        <li class="menu-title">Home Multipage</li>
                                                                        <li><a href="/reference/index">Business Main</a> </li>
                                                                        <li><a href="/reference/index2">Business 3</a> </li>
                                                                        <li><a href="/reference/index3">Consulting 2</a> </li>
                                                                        <li><a href="/reference/index4">Consulting 1</a> </li>
                                                                        <li><a href="/reference/index5">Business 2</a> </li>
                                                                        <li><a href="/reference/index6">Insurance</a> </li>
                                                                        <li><a href="/reference/index7">Saas</a> </li>
                                                                        <li><a href="/reference/index8">Human Resource</a> </li>
                                                                        <li><a href="/reference/index9">Digital Agency</a> </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="single-megamenu">
                                                                    <ul class="sub-menu">
                                                                        <li class="menu-title">Home Multipage</li>
                                                                        <li><a href="/reference/index10">Law Firm</a></li>
                                                                        <li><a href="/reference/index11">Consulting 3</a></li>
                                                                        <li><a href="/reference/index12">Digital Agency(SEO)</a></li>
                                                                        <li><a href="/reference/index13">Digital Agency</a></li>
                                                                        <li><a href="/reference/index14">Corporate Business</a></li>
                                                                        <li><a href="/reference/index15">App Landing</a></li>
                                                                        <li><a href="/reference/index16">It Solutions</a></li>
                                                                        <li><a href="/reference/index17">Marketing Agency</a></li>
                                                                        <li class="last-item"><a href="/reference/index18">Corporate Business 2</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="single-megamenu">
                                                                 <ul class="sub-menu last-sub-menu">
                                                                     <li class="menu-title">Home Onepage</li>
                                                                     <li><a href="/reference/onepage1">Onepage 1</a> </li>
                                                                     <li><a href="/reference/onepage2">Onepage 2</a> </li>
                                                                     <li><a href="/reference/onepage3">Onepage 3</a> </li>
                                                                     <li><a href="/reference/onepage4">Onepage 4</a> </li>
                                                                     <li><a href="/reference/onepage5">Onepage 5</a> </li>
                                                                     <li><a href="/reference/onepage6">Onepage 6</a> </li>                        
                                                                     <li><a href="/reference/onepage8">Onepage 8</a> </li>
                                                                     <li><a href="/reference/onepage9">Onepage 9</a> </li>
                                                                     <li><a href="/reference/onepage10">Onepage 10</a> </li>
                                                                 </ul>
                                                             </div>
                                                             <div class="single-megamenu">
                                                                 <ul class="sub-menu last-sub-menu">
                                                                     <li class="menu-title">Home Onepage</li>
                                                                     <li><a href="/reference/onepage11">Onepage 11</a> </li>
                                                                     <li><a href="/reference/onepage12">Onepage 12</a> </li>
                                                                     <li><a href="/reference/onepage13">Onepage 13</a> </li>
                                                                     <li><a href="/reference/onepage14">Onepage 14</a> </li>
                                                                     <li><a href="/reference/onepage15">Onepage 15</a> </li>
                                                                     <li><a href="/reference/onepage16">Onepage 16</a> </li>
                                                                     <li><a href="/reference/onepage17">Onepage 17</a> </li>
                                                                     <li><a href="#">Coming Soon</a> </li>
                                                                     <li class="last-item"><a href="#">Coming Soon</a> </li>
                                                                 </ul>
                                                             </div>
                                                            </div>
                                                        </li>
                                                    </ul> <!-- //.mega-menu --> 
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Pages</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/about">About Us</a></li>
                                                        <li class="menu-item-has-children">
                                                            <a href="#">Services</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/services">Services 1</a> </li>
                                                                <li class="last-item"><a href="/reference/services2">Services 2</a> </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="/reference/services-single">Services Single</a></li>
                                                        <li class="menu-item-has-children">
                                                            <a href="#">Our Peoples</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/team">Team Grid Style1</a> </li>
                                                                <li class="last-item"><a href="/reference/team2">Team Grid Style2</a> </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="/reference/price-tables">Price Tables</a></li>
                                                        <li class="last-item"><a href="/reference/faq">Faq</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Portfolios</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/portfolio">Portfolio One</a></li>
                                                        <li><a href="/reference/portfolio2">Portfolio Two</a></li>
                                                        <li><a href="/reference/portfolio3">Portfolio Three</a></li>
                                                        <li><a href="/reference/portfolio4">Portfolio Four</a></li>
                                                        <li><a href="/reference/portfolio5">Portfolio Five</a></li>
                                                        <li><a href="/reference/portfolio6">Portfolio Six</a></li>
                                                        <li class="last-item menu-item-has-children">
                                                            <a href="#">Portfolio Single</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/portfolio-standard">Standard Style</a> </li>
                                                                <li><a href="/reference/portfolio-slider">Slider Style</a> </li>
                                                                <li class="last-item"><a href="/reference/portfolio-gallery">Gallery Style</a> </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Blog</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/blog">Blog</a></li>
                                                        <li class="last-item"><a href="/reference/blog-single">Blog Single</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Shop</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/shop">Shop</a></li>
                                                        <li><a href="/reference/shop-single">Shop Single</a></li>
                                                        <li><a href="/reference/cart">Cart</a></li>
                                                        <li><a href="/reference/checkout">Checkout</a></li>
                                                        <li class="last-item"><a href="/reference/account">My Account</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children current-menu-item">
                                                    <a href="#">Contact</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/contact">Contact 1</a></li>
                                                        <li class="last-item active"><a href="/reference/contact2">Contact 2</a></li>
                                                    </ul>
                                                </li>
                                            </ul> <!-- //.nav-menu -->
                                        </nav>
                                    </div> <!-- //.main-menu -->
                                    <div class="expand-btn-inner">
                                        <ul>
                                            <li class="search-parent">
                                                <a class="hidden-xs rs-search" data-bs-toggle="modal" data-bs-target="#searchModal" href="#">
                                                    <i class="flaticon-search"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a id="nav-expander" class="humburger nav-expander" href="#">
                                                    <span class="dot1"></span>
                                                    <span class="dot2"></span>
                                                    <span class="dot3"></span>
                                                    <span class="dot4"></span>
                                                    <span class="dot5"></span>
                                                    <span class="dot6"></span>
                                                    <span class="dot7"></span>
                                                    <span class="dot8"></span>
                                                    <span class="dot9"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu End -->

                <!-- Canvas Menu start -->
                <nav class="right_menu_togle hidden-md">
                    <div class="close-btn">
                        <span id="nav-close" class="humburger">
                            <span class="dot1"></span>
                            <span class="dot2"></span>
                            <span class="dot3"></span>
                            <span class="dot4"></span>
                            <span class="dot5"></span>
                            <span class="dot6"></span>
                            <span class="dot7"></span>
                            <span class="dot8"></span>
                            <span class="dot9"></span>
                        </span>
                    </div>
                    <div class="canvas-logo">
                        <a href="/reference/index"><img src="assets/images/logo-dark.png" alt="logo"></a>
                    </div>
                    <div class="offcanvas-text">
                        <p>Consectetur adipiscing elit. Duis at dictum risus, non suscip it arcu. Quisque aliquam posuere tortor aliquam posuere tortor develop database.</p>
                    </div>
                    <div class="canvas-contact">
                        <ul class="contact">
                            <li><i class="flaticon-location"></i> 374 William S Canning Blvd, Fall River MA 2721, USA</li>
                            <li><i class="flaticon-call"></i><a href="tel:+880155-69569">(+880)155-69569</a></li>
                            <li><i class="flaticon-email"></i><a href="mailto:support@rstheme.com">support@rstheme.com</a></li>
                            <li><i class="flaticon-clock"></i>10:00 - 17:00</li>
                        </ul>
                        <ul class="social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Canvas Menu end -->
            </header>
            <!--Header End-->
        </div>
        <!--Full width header End-->

		<!-- Main content Start -->
        <div class="main-content">
            <!-- Contact Section Start -->
            <div id="rs-contact" class="rs-contact style1 inner">
                <div class="g-map modify">
                    <iframe src="https://maps.google.com/maps?q=37.803467%2C%20-122.472369&t=&z=13&ie=UTF8&iwloc=&output=embed"></iframe>
                </div>
                <div class="gray-bg pt-100 pb-100 md-pt-80 md-pb-80">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 form-part white-bg">
                                <div class="sec-title mb-45">
                                    <div class="sub-title primary">CONTACT US</div>
                                    <h2 class="title mb-0">Get In Touch</h2>
                                </div>
                                <div id="form-messages"></div>
                                <form id="contact-form" class="contact-form" method="post" action="mailer.php">
                                    <div class="row">
                                        <div class="col-md-6 mb-30">
                                            <div class="common-control form-group mb-0">
                                                <input type="text" name="name" placeholder="Name" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-30">
                                            <div class="common-control form-group mb-0">
                                                <input type="email" name="email" placeholder="Email" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-30">
                                            <div class="common-control form-group mb-0">
                                                <input type="text" name="phone" placeholder="Phone Number" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-30">
                                            <div class="common-control form-group mb-0">
                                                <input type="text" name="website" placeholder="Your Website" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-30">
                                            <div class="common-control form-group mb-0">
                                                <textarea name="message" placeholder="Your Message Here" required=""></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="submit-btn form-group mb-0">
                                                <button type="submit" class="readon">Submit Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-4 pl-0 md-pr-0 md-order-first">
                                <div class="contact-info">
                                    <h3 class="title">Contact Info</h3>
                                    <div class="info-wrap mb-20">
                                        <div class="icon-part">
                                            <i class="flaticon-location"></i>
                                        </div>
                                        <div class="content-part">
                                            <h4>Office Address</h4>
                                            127 Double Street, Dublin, United Kingdom.
                                        </div>
                                    </div>
                                    <div class="info-wrap mb-20">
                                        <div class="icon-part">
                                            <i class="flaticon-call"></i>
                                        </div>
                                        <div class="content-part">
                                            <h4>Telephone</h4>
                                            <p>P: <a href="tel:+1235558888">(+123) 555 8888</a></p>
                                            <p>P: <a href="tel:+1235558899">(+123) 555 8899</a></p>
                                        </div>
                                    </div>
                                    <div class="info-wrap mb-20">
                                        <div class="icon-part">
                                            <i class="flaticon-email"></i>
                                        </div>
                                        <div class="content-part">
                                            <h4>Mail Us</h4>
                                            <p>E: <a href="mailto:support@rstheme.com">support@rstheme.com</a></p>
                                            <p>E: <a href="mailto:info@codesless.com">info@codesless.com</a></p>
                                        </div>
                                    </div>
                                    <div class="info-wrap">
                                        <div class="icon-part">
                                            <i class="flaticon-clock"></i>
                                        </div>
                                        <div class="content-part">
                                            <h4>Opening Hours</h4>
                                            <p>Mon-Fri: 10:00-18:00</p>
                                            <p>Sat-Sun: 10:00-14:00</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Contact Section End -->
        </div> 
        <!-- Main content End -->

        <!-- Footer Start -->
        <footer id="rs-footer" class="rs-footer">
            <div class="container">
                <div class="footer-newsletter">
                    <div class="row y-middle">
                        <div class="col-md-6 sm-mb-26">
                            <h3 class="title white-color mb-0">Newsletter Subscribe</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <form class="newsletter-form">
                                <input type="email" name="email" placeholder="Your email address" required="">
                                <button type="submit"><i class="fa fa-paper-plane"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer-content pt-62 pb-79 md-pb-64 sm-pt-48">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-widget md-mb-39">
                            <div class="about-widget pr-15">
                                <div class="logo-part">
                                    <a href="/reference/index"><img src="assets/images/logo.png" alt="Footer Logo"></a>
                                </div>
                                <p class="desc">We denounce with righteous indignation in and dislike men who are so beguiled and to demo realized by the charms of pleasure moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound.</p>
                                <div class="btn-part">
                                    <a class="readon" href="about.html">Discover More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 md-mb-32 footer-widget">
                            <h4 class="widget-title">Contact Info</h4>
                            <ul class="address-widget pr-40">
                                <li>
                                    <i class="flaticon-location"></i>
                                    <div class="desc">374 William S Canning Blvd, Fall River MA 2721, USA</div>
                                </li>
                                <li>
                                    <i class="flaticon-call"></i>
                                    <div class="desc">
                                        <a href="tel:+8801739753105">(+880)173-9753105</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-email"></i>
                                    <div class="desc">
                                        <a href="mailto:support@rstheme.com">support@rstheme.com</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-clock"></i>
                                    <div class="desc">
                                        10:00 - 17:00
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-widget">
                            <h4 class="widget-title">Latest Posts</h4>
                            <div class="footer-post">
                                <div class="post-wrap mb-15">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/1.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Covid-19 threatens the next generation of smartphones</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            September 6, 2019
                                        </div>
                                    </div>
                                </div>
                                <div class="post-wrap mb-15">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/2.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Soundtrack filma Lady Exclusive Music</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            April 15, 2019
                                        </div>
                                    </div>
                                </div>
                                <div class="post-wrap">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/3.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Winged moved stars, fruit creature seed night.</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            October 9, 2019
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row y-middle">
                        <div class="col-lg-6 col-md-8 sm-mb-21">
                            <div class="copyright">
                                <p>© Copyright 2021 Reobiz. All Rights Reserved.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-4 text-right sm-text-center">
                            <ul class="footer-social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div id="scrollUp">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- End scrollUp  -->

        <!-- Search Modal Start -->
        <div class="modal fade search-modal" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
            <button type="button" class="close" data-bs-dismiss="modal">
                <span class="flaticon-cross"></span>
            </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here..." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <script>
        
        </script>
        <!-- modernizr js -->
        <script src="assets/js/modernizr-2.8.3.min.js"></script>
        <!-- jquery latest version -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap v4.4.1 js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Menu js -->
        <script src="assets/js/rsmenu-main.js"></script> 
        <!-- op nav js -->
        <script src="assets/js/jquery.nav.js"></script>
        <!-- owl.carousel js -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Slick js -->
        <script src="assets/js/slick.min.js"></script>
        <!-- isotope.pkgd.min js -->
        <script src="assets/js/isotope.pkgd.min.js"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="assets/js/imagesloaded.pkgd.min.js"></script>
        <!-- wow js -->
        <script src="assets/js/wow.min.js"></script>
        <!-- aos js -->
        <script src="assets/js/aos.js"></script>
        <!-- Skill bar js -->
        <script src="assets/js/skill.bars.jquery.js"></script>
        <script src="assets/js/jquery.counterup.min.js"></script>        
         <!-- counter top js -->
        <script src="assets/js/waypoints.min.js"></script>
        <!-- video js -->
        <script src="assets/js/jquery.mb.YTPlayer.min.js"></script>
        <!-- magnific popup js -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Nivo slider js -->
        <script src="assets/inc/custom-slider/js/jquery.nivo.slider.js"></script>
        <!-- plugins js -->
        <script src="assets/js/plugins.js"></script>
        <!-- contact form js -->
        <script src="assets/js/contact.form.js"></script>
        <!-- main js -->
        <script src="assets/js/main.js"></script>
    </body>
</html>