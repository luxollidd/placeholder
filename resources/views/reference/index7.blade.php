<!DOCTYPE html>
<html lang="zxx">
    <head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>Saas - Reobiz – Consulting Business HTML Template</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/fav.png">
        <!-- Bootstrap v4.4.1 css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
        <!-- aos css -->
        <link rel="stylesheet" type="text/css" href="assets/css/aos.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href="assets/css/off-canvas.css">
        <!-- linea-font css -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/linea-fonts.css">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/flaticon.css">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">
        <!-- Main Menu css -->
        <link rel="stylesheet" href="assets/css/rsmenu-main.css">
        <!-- nivo slider CSS -->
        <link rel="stylesheet" type="text/css" href="assets/inc/custom-slider/css/nivo-slider.css">
        <link rel="stylesheet" type="text/css" href="assets/inc/custom-slider/css/preview.css">
        <!-- rsmenu transitions css -->
        <link rel="stylesheet" href="assets/css/rsmenu-transitions.css">
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href="assets/css/rs-spacing.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="style.css"> <!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    </head>
    <body class="home-seven">

        <!-- Preloader area start here -->
        <div id="loader" class="loader">
            <div class="spinner"></div>
        </div>
        <!--End preloader here -->

        <!--Full width header Start-->
        <div class="full-width-header header-style2 modify">
            <!--Header Start-->
            <header id="rs-header" class="rs-header">
                <!-- Menu Start -->
                <div class="menu-area menu-sticky">
                    <div class="container">
                        <div class="row y-middle">
                            <div class="col-lg-3">
                                <div class="logo-area">
                                    <a class="dark" href="index.html"><img src="assets/images/logo-dark.png" alt="logo"></a>
                                    <a class="light" href="index.html"><img src="assets/images/logo-light.png" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-9 text-right">
                                <div class="rs-menu-area">
                                    <div class="main-menu">
                                        <div class="mobile-menu">
                                            <a class="rs-menu-toggle">
                                                <i class="fa fa-bars"></i>
                                            </a>
                                        </div>
                                        <nav class="rs-menu">
                                            <ul class="nav-menu">
                                                <li class="rs-mega-menu mega-rs menu-item-has-children current-menu-item"> <a href="#">Home</a>
                                                    <ul class="mega-menu"> 
                                                        <li class="mega-menu-container">
                                                            <div class="mega-menu-innner">
                                                                <div class="single-megamenu">
                                                                    <ul class="sub-menu">
                                                                        <li class="menu-title">Home Multipage</li>
                                                                        <li><a href="/reference/index">Business Main</a> </li>
                                                                        <li><a href="/reference/index2">Business 3</a> </li>
                                                                        <li><a href="/reference/index3">Consulting 2</a> </li>
                                                                        <li><a href="/reference/index4">Consulting 1</a> </li>
                                                                        <li><a href="/reference/index5">Business 2</a> </li>
                                                                        <li><a href="/reference/index6">Insurance</a> </li>
                                                                        <li class="active"><a href="/reference/index7">Saas</a> </li>
                                                                        <li><a href="/reference/index8">Human Resource</a> </li>
                                                                        <li><a href="/reference/index9">Digital Agency</a> </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="single-megamenu">
                                                                    <ul class="sub-menu">
                                                                        <li class="menu-title">Home Multipage</li>
                                                                        <li><a href="/reference/index10">Law Firm</a></li>
                                                                        <li><a href="/reference/index11">Consulting 3</a></li>
                                                                        <li><a href="/reference/index12">Digital Agency(SEO)</a></li>
                                                                        <li><a href="/reference/index13">Digital Agency</a></li>
                                                                        <li><a href="/reference/index14">Corporate Business</a></li>
                                                                        <li><a href="/reference/index15">App Landing</a></li>
                                                                        <li><a href="/reference/index16">It Solutions</a></li>
                                                                        <li><a href="/reference/index17">Marketing Agency</a></li>
                                                                        <li class="last-item"><a href="/reference/index18">Corporate Business 2</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="single-megamenu">
                                                                 <ul class="sub-menu last-sub-menu">
                                                                     <li class="menu-title">Home Onepage</li>
                                                                     <li><a href="/reference/onepage1">Onepage 1</a> </li>
                                                                     <li><a href="/reference/onepage2">Onepage 2</a> </li>
                                                                     <li><a href="/reference/onepage3">Onepage 3</a> </li>
                                                                     <li><a href="/reference/onepage4">Onepage 4</a> </li>
                                                                     <li><a href="/reference/onepage5">Onepage 5</a> </li>
                                                                     <li><a href="/reference/onepage6">Onepage 6</a> </li>                        
                                                                     <li><a href="/reference/onepage8">Onepage 8</a> </li>
                                                                     <li><a href="/reference/onepage9">Onepage 9</a> </li>
                                                                     <li><a href="/reference/onepage10">Onepage 10</a> </li>
                                                                 </ul>
                                                             </div>
                                                             <div class="single-megamenu">
                                                                 <ul class="sub-menu last-sub-menu">
                                                                     <li class="menu-title">Home Onepage</li>
                                                                     <li><a href="/reference/onepage11">Onepage 11</a> </li>
                                                                     <li><a href="/reference/onepage12">Onepage 12</a> </li>
                                                                     <li><a href="/reference/onepage13">Onepage 13</a> </li>
                                                                     <li><a href="/reference/onepage14">Onepage 14</a> </li>
                                                                     <li><a href="/reference/onepage15">Onepage 15</a> </li>
                                                                     <li><a href="/reference/onepage16">Onepage 16</a> </li>
                                                                     <li><a href="/reference/onepage17">Onepage 17</a> </li>
                                                                     <li><a href="#">Coming Soon</a> </li>
                                                                     <li class="last-item"><a href="#">Coming Soon</a> </li>
                                                                 </ul>
                                                             </div>
                                                            </div>
                                                        </li>
                                                    </ul> <!-- //.mega-menu --> 
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Pages</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/about">About Us</a></li>
                                                        <li class="menu-item-has-children">
                                                            <a href="#">Services</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/services">Services 1</a> </li>
                                                                <li class="last-item"><a href="/reference/services2">Services 2</a> </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="/reference/services-single">Services Single</a></li>
                                                        <li class="menu-item-has-children">
                                                            <a href="#">Our Peoples</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/team">Team Grid Style1</a> </li>
                                                                <li class="last-item"><a href="/reference/team2">Team Grid Style2</a> </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="/reference/price-tables">Price Tables</a></li>
                                                        <li class="last-item"><a href="/reference/faq">Faq</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Portfolios</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/portfolio">Portfolio One</a></li>
                                                        <li><a href="/reference/portfolio2">Portfolio Two</a></li>
                                                        <li><a href="/reference/portfolio3">Portfolio Three</a></li>
                                                        <li><a href="/reference/portfolio4">Portfolio Four</a></li>
                                                        <li><a href="/reference/portfolio5">Portfolio Five</a></li>
                                                        <li><a href="/reference/portfolio6">Portfolio Six</a></li>
                                                        <li class="last-item menu-item-has-children">
                                                            <a href="#">Portfolio Single</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/portfolio-standard">Standard Style</a> </li>
                                                                <li><a href="/reference/portfolio-slider">Slider Style</a> </li>
                                                                <li class="last-item"><a href="/reference/portfolio-gallery">Gallery Style</a> </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Blog</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/blog">Blog</a></li>
                                                        <li class="last-item"><a href="/reference/blog-single">Blog Single</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Shop</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/shop">Shop</a></li>
                                                        <li><a href="/reference/shop-single">Shop Single</a></li>
                                                        <li><a href="/reference/cart">Cart</a></li>
                                                        <li><a href="/reference/checkout">Checkout</a></li>
                                                        <li class="last-item"><a href="/reference/account">My Account</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Contact</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/contact">Contact 1</a></li>
                                                        <li class="last-item"><a href="/reference/contact2">Contact 2</a></li>
                                                    </ul>
                                                </li>
                                            </ul> <!-- //.nav-menu -->
                                        </nav>
                                    </div> <!-- //.main-menu -->
                                    <div class="expand-btn-inner">
                                        <ul>
                                            <li class="search-parent">
                                                <a class="hidden-xs rs-search" data-bs-toggle="modal" data-bs-target="#searchModal" href="#">
                                                    <i class="flaticon-search"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu End -->
            </header>
            <!--Header End-->
        </div>
        <!--Full width header End-->

		<!-- Main content Start -->
        <div class="main-content">
            <!-- Banner Section Start -->
            <div class="rs-banner style1">
                <div class="container">
                    <div class="content-part text-center">
                        <h2 class="title white-color">A System to Boost Productivity And Profitability of Your Business.</h2>
                        <form class="subscribe-form">
                            <input type="email" name="email" placeholder="Your email address" required>
                            <button type="submit">Subscribe</button>
                        </form>
                        <p class="desc mb-0 white-color">The best saas business theme on your business </p>
                    </div>
                    <div class="parallax-elemen" id="stuff">
                        <span data-depth="0.5"><img class="wow animate slideInLeft" src="assets/images/social/1.png" alt=""></span>
                        <span data-depth="0.4"><img class="wow animate slideInRight" data-wow-delay="100ms" src="assets/images/social/2.png" alt=""></span>
                        <span data-depth="0.6"><img class="wow animate slideInRight" data-wow-delay="500ms" src="assets/images/social/3.png" alt=""></span>
                        <span data-depth="1.2"><img class="wow animate slideInLeft" data-wow-delay="400ms" src="assets/images/social/4.png" alt=""></span>
                        <span data-depth="1"><img class="wow animate slideInLeft" data-wow-delay="200ms" src="assets/images/social/5.png" alt=""></span>
                        <span data-depth="0.8"><img class="wow animate slideInRight" data-wow-delay="300ms" src="assets/images/social/6.png" alt=""></span>

                        <div data-depth="0.3"><img src="assets/images/banner/h7-bnr-lyr.png" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Banner Section End -->

            <!-- Services Mini Section Start -->
            <div class="rs-services style8 gray-bg3 pt-83 pb-70 md-pt-67 md-pb-50">
                <div class="container">
                    <div class="sec-title mb-38 md-mb-41">
                        <h2 class="title mb-0">Secure Productivity For The Business</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 mb-30">
                            <div class="service-wrap">
                                <div class="icon-part one">
                                    <img src="assets/images/services/icons/style8/1.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="/reference/services-single">Built Service Business</a></h4>
                                    <p class="desc mb-20">We always provide people a complete solution focused of any business.</p>
                                    <div class="read-btn">
                                        <a href="/reference/services-single">Read More <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-30">
                            <div class="service-wrap">
                                <div class="icon-part two">
                                    <img src="assets/images/services/icons/style8/2.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="/reference/services-single">Built Service Business</a></h4>
                                    <p class="desc mb-20">We always provide people a complete solution focused of any business.</p>
                                    <div class="read-btn">
                                        <a href="/reference/services-single">Read More <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-30">
                            <div class="service-wrap">
                                <div class="icon-part three">
                                    <img src="assets/images/services/icons/style8/3.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="/reference/services-single">Built Service Business</a></h4>
                                    <p class="desc mb-20">We always provide people a complete solution focused of any business.</p>
                                    <div class="read-btn">
                                        <a href="/reference/services-single">Read More <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Services Mini Section End -->

            <!-- Why Choose Us Section Start -->
            <div id="rs-whychooseus" class="rs-whychooseus style3 pt-82 pb-95 md-pt-40 md-pb-72">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 pr-70 lg-pr-18 md-pr-15">
                            <div class="sec-title mb-43">
                                <h2 class="title mb-0">Why Choose Reobiz</h2>
                            </div>
                            <div class="content-wrap">
                                <div class="item-part mb-30">
                                    <div class="icon-part">
                                        <img src="assets/images/whychooseus/style3/icons/1.png" alt="">
                                    </div>
                                    <div class="desc-text">
                                        <h4 class="title">Modern Deshboard!</h4>
                                        <div class="desc">Neque porro quisquam est, qui dolorem ipsum quia.</div>
                                    </div>
                                </div>
                                <div class="item-part mb-30">
                                    <div class="icon-part">
                                        <img src="assets/images/whychooseus/style3/icons/2.png" alt="">
                                    </div>
                                    <div class="desc-text">
                                        <h4 class="title">Rebuilding business goals</h4>
                                        <div class="desc">Neque porro quisquam est, qui dolorem ipsum quia.</div>
                                    </div>
                                </div>
                                <div class="item-part">
                                    <div class="icon-part">
                                        <img src="assets/images/whychooseus/style3/icons/3.png" alt="">
                                    </div>
                                    <div class="desc-text">
                                        <h4 class="title">Track all orders</h4>
                                        <div class="desc">Neque porro quisquam est, qui dolorem ipsum quia.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="offset-lg-1 col-lg-6 md-order-first md-text-center md-mb-40">
                            <div class="image-wrap">
                                <img src="assets/images/whychooseus/style3/img.png" alt="">
                                <img class="ly ly1" src="assets/images/whychooseus/style3/ly1.png" alt="">
                                <img class="ly ly2" src="assets/images/whychooseus/style3/ly2.png" alt="">
                                <img class="ly ly3" src="assets/images/whychooseus/style3/ly3.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Why Choose Us Section End -->

            <!-- Pricing Plan Section Start -->
            <div class="rs-pricing style2 pt-83 pb-100 md-pt-67 md-pb-80">
                <div class="container">
                    <div class="sec-title mb-53 md-mb-40 text-center">
                        <h2 class="title mb-0">Choose Your Plan</h2>
                    </div>
                    <div class="row gutter-20">
                        <div class="col-lg-4 md-mb-30">
                            <div class="pricing-wrap one">
                                <div class="icon-part">
                                    <img src="assets/images/pricing/1.png" alt="">
                                </div>
                                <div class="top-part">
                                    <h3 class="title">Basic</h3>
                                    <div class="price"><span>$</span>9.99</div>
                                    <span class="priod">Per Month</span>
                                </div>
                                <ul class="middle-part">
                                    <li><i class="fa fa-check"></i>Standard Feature</li>
                                    <li><i class="fa fa-check"></i>Another Great Feature</li>
                                    <li><i class="fa fa-close"></i>Obsolete Feature</li>
                                    <li><i class="fa fa-check"></i>Exciting Feature</li>
                                </ul>
                                <div class="btn-part">
                                    <a href="#">Buy Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 md-mb-30">
                            <div class="pricing-wrap two">
                                <div class="icon-part">
                                    <img src="assets/images/pricing/2.png" alt="">
                                </div>
                                <div class="top-part">
                                    <h3 class="title">Advanced</h3>
                                    <div class="price"><span>$</span>29.99</div>
                                    <span class="priod">Per Month</span>
                                </div>
                                <ul class="middle-part">
                                    <li><i class="fa fa-check"></i>Standard Feature</li>
                                    <li><i class="fa fa-check"></i>Another Great Feature</li>
                                    <li><i class="fa fa-close"></i>Obsolete Feature</li>
                                    <li><i class="fa fa-check"></i>Exciting Feature</li>
                                </ul>
                                <div class="btn-part">
                                    <a href="#">Buy Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="pricing-wrap three">
                                <div class="icon-part">
                                    <img src="assets/images/pricing/3.png" alt="">
                                </div>
                                <div class="top-part">
                                    <h3 class="title">Professional</h3>
                                    <div class="price"><span>$</span>59.99</div>
                                    <span class="priod">Per Month</span>
                                </div>
                                <ul class="middle-part">
                                    <li><i class="fa fa-check"></i>Standard Feature</li>
                                    <li><i class="fa fa-check"></i>Another Great Feature</li>
                                    <li><i class="fa fa-close"></i>Obsolete Feature</li>
                                    <li><i class="fa fa-check"></i>Exciting Feature</li>
                                </ul>
                                <div class="btn-part">
                                    <a href="#">Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Pricing Plan Section End -->

            <!-- Testimonial Section Start -->
            <div class="rs-testimonial style5 pt-85 md-pt-65">
                <div class="container">
                    <div class="rs-carousel owl-carousel dot-style1" data-loop="true" data-items="1" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="1" data-lg-device="1" data-md-device-nav="false" data-md-device-dots="false">
                        <div class="content-wrap">
                            <div class="icon-part">
                                <img src="assets/images/quote.png" alt="">
                            </div>
                            <div class="desc">Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway.</div>
                            <div class="posted-by">
                                <h3 class="name">Monty Moni</h3>
                                <span class="designation">CEO, Keen IT Solution</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Partner Section Start -->
                <div class="rs-partner modify4 pt-80 pb-90 md-pt-70 md-pb-70">
                    <div class="container">
                        <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="2" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="3" data-lg-device="3" data-md-device-nav="false" data-md-device-dots="false">
                            <div class="partner-item">
                                <a href="https://rstheme.com"><img src="assets/images/partner/light2/1.png" alt=""></a>
                            </div>
                            <div class="partner-item">
                                <a href="https://rstheme.com"><img src="assets/images/partner/light2/2.png" alt=""></a>
                            </div>
                            <div class="partner-item">
                                <a href="https://rstheme.com"><img src="assets/images/partner/light2/3.png" alt=""></a>
                            </div>
                            <div class="partner-item">
                                <a href="https://rstheme.com"><img src="assets/images/partner/light2/4.png" alt=""></a>
                            </div>
                            <div class="partner-item">
                                <a href="https://rstheme.com"><img src="assets/images/partner/light2/5.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Partner Section End -->
            </div>
            <!-- Testimonial Section End -->

            <!-- Decoration Section Start -->
            <div class="rs-decoration style1 pt-100 md-pt-65">
                <div class="container">
                    <div class="sec-title text-center">
                        <h2 class="title mb-0" data-wow-delay="300ms" data-wow-duration="2000ms">Seamless Integrations With <br> Leading Platforms</h2>
                    </div>
                    <div class="parallax-elemen" id="stuff2">
                        <span data-depth="0.5"><img class="wow animate zoomIn" src="assets/images/social/1.png" alt=""></span>
                        <span data-depth="0.4"><img class="wow animate zoomIn" data-wow-delay="100ms" src="assets/images/social/2.png" alt=""></span>
                        <span data-depth="0.6"><img class="wow animate zoomIn" data-wow-delay="400ms" src="assets/images/social/3.png" alt=""></span>
                        <span data-depth="0.8"><img class="wow animate zoomIn" data-wow-delay="300ms" src="assets/images/social/4.png" alt=""></span>
                        <span data-depth="1"><img class="wow animate zoomIn" data-wow-delay="200ms"  src="assets/images/social/5.png" alt=""></span>
                        <span data-depth="0.8"><img class="wow animate zoomIn" src="assets/images/social/6.png" alt=""></span>
                        <span data-depth="0.5"><img class="wow animate zoomIn" data-wow-delay="100ms" src="assets/images/social/1.png" alt=""></span>
                        <span data-depth="0.4"><img class="wow animate zoomIn" data-wow-delay="400ms" src="assets/images/social/2.png" alt=""></span>
                        <span data-depth="0.6"><img class="wow animate zoomIn" data-wow-delay="300ms" src="assets/images/social/3.png" alt=""></span>
                        <div class="main-image"><img src="assets/images/decoration/integrate.png" alt=""></div>
                    </div>
                </div>
            </div>
            <!-- Decoration Section End -->
        </div> 
        <!-- Main content End -->

        <!-- Footer Start -->
        <footer id="rs-footer" class="rs-footer">
            <div class="container">
                <div class="footer-content pt-80 pb-79 md-pb-64">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-widget md-mb-39">
                            <div class="about-widget pr-15">
                                <div class="logo-part">
                                    <a href="/reference/index"><img src="assets/images/logo-light.png" alt="Footer Logo"></a>
                                </div>
                                <p class="desc">We denounce with righteous indignation in and dislike men who are so beguiled and to demo realized by the charms of pleasure moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound.</p>
                                <div class="btn-part">
                                    <a class="readon" href="about.html">Discover More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 md-mb-32 footer-widget">
                            <h4 class="widget-title">Contact Info</h4>
                            <ul class="address-widget pr-40">
                                <li>
                                    <i class="flaticon-location"></i>
                                    <div class="desc">374 William S Canning Blvd, Fall River MA 2721, USA</div>
                                </li>
                                <li>
                                    <i class="flaticon-call"></i>
                                    <div class="desc">
                                        <a href="tel:+8801739753105">(+880)173-9753105</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-email"></i>
                                    <div class="desc">
                                        <a href="mailto:support@rstheme.com">support@rstheme.com</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-clock"></i>
                                    <div class="desc">
                                        10:00 - 17:00
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-widget">
                            <h4 class="widget-title">Latest Posts</h4>
                            <div class="footer-post">
                                <div class="post-wrap mb-15">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/1.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Covid-19 threatens the next generation of smartphones</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            September 6, 2019
                                        </div>
                                    </div>
                                </div>
                                <div class="post-wrap mb-15">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/2.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Soundtrack filma Lady Exclusive Music</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            April 15, 2019
                                        </div>
                                    </div>
                                </div>
                                <div class="post-wrap">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/3.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Winged moved stars, fruit creature seed night.</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            October 9, 2019
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row y-middle">
                        <div class="col-lg-6 col-md-8 sm-mb-21">
                            <div class="copyright">
                                <p>© Copyright 2021 Reobiz. All Rights Reserved.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-4 text-right sm-text-center">
                            <ul class="footer-social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div id="scrollUp">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- End scrollUp  -->

        <!-- Search Modal Start -->
        <div class="modal fade search-modal" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
            <button type="button" class="close" data-bs-dismiss="modal">
                <span class="flaticon-cross"></span>
            </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here..." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- modernizr js -->
        <script src="assets/js/modernizr-2.8.3.min.js"></script>
        <!-- jquery latest version -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap v4.4.1 js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Menu js -->
        <script src="assets/js/rsmenu-main.js"></script> 
        <!-- op nav js -->
        <script src="assets/js/jquery.nav.js"></script>
        <!-- owl.carousel js -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Slick js -->
        <script src="assets/js/slick.min.js"></script>
        <!-- isotope.pkgd.min js -->
        <script src="assets/js/isotope.pkgd.min.js"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="assets/js/imagesloaded.pkgd.min.js"></script>
        <!-- wow js -->
        <script src="assets/js/wow.min.js"></script>
        <!-- aos js -->
        <script src="assets/js/aos.js"></script>
        <!-- Skill bar js -->
        <script src="assets/js/skill.bars.jquery.js"></script>
        <script src="assets/js/jquery.counterup.min.js"></script>         
         <!-- counter top js -->
        <script src="assets/js/waypoints.min.js"></script>
        <!-- video js -->
        <script src="assets/js/jquery.mb.YTPlayer.min.js"></script>
        <!-- magnific popup js -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Nivo slider js -->
        <script src="assets/inc/custom-slider/js/jquery.nivo.slider.js"></script>
        <!-- plugins js -->
        <script src="assets/js/plugins.js"></script>
        <!-- parallax-effect js -->
        <script src="assets/js/parallax-effect.min.js"></script>
        <!-- contact form js -->
        <script src="assets/js/contact.form.js"></script>
        <!-- ProgressBar Js -->
        <script src="assets/js/jQuery-plugin-progressbar.js"></script>
        <!-- main js -->
        <script src="assets/js/main.js"></script>
    </body>
</html>