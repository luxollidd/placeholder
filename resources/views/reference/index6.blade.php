<!DOCTYPE html>
<html lang="zxx">
    <head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>Insurance - Reobiz – Consulting Business HTML Template</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/fav.png">
        <!-- Bootstrap v4.4.1 css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
        <!-- aos css -->
        <link rel="stylesheet" type="text/css" href="assets/css/aos.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href="assets/css/off-canvas.css">
        <!-- linea-font css -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/linea-fonts.css">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/flaticon.css">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">
        <!-- Main Menu css -->
        <link rel="stylesheet" href="assets/css/rsmenu-main.css">        
        <!-- Rs animations css -->
        <link rel="stylesheet" href="assets/css/rs-animations.css">
        <!-- nivo slider CSS -->
        <link rel="stylesheet" type="text/css" href="assets/inc/custom-slider/css/nivo-slider.css">
        <link rel="stylesheet" type="text/css" href="assets/inc/custom-slider/css/preview.css">
        <!-- rsmenu transitions css -->
        <link rel="stylesheet" href="assets/css/rsmenu-transitions.css">
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href="assets/css/rs-spacing.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="style.css"> <!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    </head>
    <body class="home-six">

        <!-- Preloader area start here -->
        <div id="loader" class="loader">
            <div class="spinner"></div>
        </div>
        <!--End preloader here -->

        <!--Full width header Start-->
        <div class="full-width-header header-style2">
            <!-- Toolbar Start -->
            <div class="toolbar-area hidden-md">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="toolbar-contact">
                                <ul>
                                    <li><i class="flaticon-email"></i><a href="mailto:info@yourwebsite.com">support@rstheme.com</a></li>
                                    <li><i class="flaticon-call"></i><a href="tel:+123456789">(+123) 456789</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="toolbar-sl-share">
                                <ul>
                                    <li class="opening"> <i class="flaticon-clock"></i> Mon - Fri: 9:00 am - 06.00pm / Closed on Weekends</li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Toolbar End -->
            <!--Header Start-->
            <header id="rs-header" class="rs-header">
                <!-- Menu Start -->
                <div class="menu-area menu-sticky">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="logo-area">
                                    <a class="dark" href="index.html"><img src="assets/images/logo-green2.png" alt="logo"></a>
                                    <a class="light" href="index.html"><img src="assets/images/logo-green.png" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-10 text-right">
                                <div class="rs-menu-area">
                                    <div class="main-menu">
                                        <div class="mobile-menu">
                                            <a class="rs-menu-toggle">
                                                <i class="fa fa-bars"></i>
                                            </a>
                                        </div>
                                        <nav class="rs-menu pr-144">
                                            <ul class="nav-menu">
                                                <li class="rs-mega-menu mega-rs menu-item-has-children current-menu-item"> <a href="#">Home</a>
                                                    <ul class="mega-menu"> 
                                                        <li class="mega-menu-container">
                                                            <div class="mega-menu-innner">
                                                                <div class="single-megamenu">
                                                                    <ul class="sub-menu">
                                                                        <li class="menu-title">Home Multipage</li>
                                                                        <li><a href="/reference/index">Business Main</a> </li>
                                                                        <li><a href="/reference/index2">Business 3</a> </li>
                                                                        <li><a href="/reference/index3">Consulting 2</a> </li>
                                                                        <li><a href="/reference/index4">Consulting 1</a> </li>
                                                                        <li><a href="/reference/index5">Business 2</a> </li>
                                                                        <li class="active"><a href="/reference/index6">Insurance</a> </li>
                                                                        <li><a href="/reference/index7">Saas</a> </li>
                                                                        <li><a href="/reference/index8">Human Resource</a> </li>
                                                                        <li><a href="/reference/index9">Digital Agency</a> </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="single-megamenu">
                                                                    <ul class="sub-menu">
                                                                        <li class="menu-title">Home Multipage</li>
                                                                        <li><a href="/reference/index10">Law Firm</a></li>
                                                                        <li><a href="/reference/index11">Consulting 3</a></li>
                                                                        <li><a href="/reference/index12">Digital Agency(SEO)</a></li>
                                                                        <li><a href="/reference/index13">Digital Agency</a></li>
                                                                        <li><a href="/reference/index14">Corporate Business</a></li>
                                                                        <li><a href="/reference/index15">App Landing</a></li>
                                                                        <li><a href="/reference/index16">It Solutions</a></li>
                                                                        <li><a href="/reference/index17">Marketing Agency</a></li>
                                                                        <li class="last-item"><a href="/reference/index18">Corporate Business 2</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="single-megamenu">
                                                                 <ul class="sub-menu last-sub-menu">
                                                                     <li class="menu-title">Home Onepage</li>
                                                                     <li><a href="/reference/onepage1">Onepage 1</a> </li>
                                                                     <li><a href="/reference/onepage2">Onepage 2</a> </li>
                                                                     <li><a href="/reference/onepage3">Onepage 3</a> </li>
                                                                     <li><a href="/reference/onepage4">Onepage 4</a> </li>
                                                                     <li><a href="/reference/onepage5">Onepage 5</a> </li>
                                                                     <li><a href="/reference/onepage6">Onepage 6</a> </li>                        
                                                                     <li><a href="/reference/onepage8">Onepage 8</a> </li>
                                                                     <li><a href="/reference/onepage9">Onepage 9</a> </li>
                                                                     <li><a href="/reference/onepage10">Onepage 10</a> </li>
                                                                 </ul>
                                                             </div>
                                                             <div class="single-megamenu">
                                                                 <ul class="sub-menu last-sub-menu">
                                                                     <li class="menu-title">Home Onepage</li>
                                                                     <li><a href="/reference/onepage11">Onepage 11</a> </li>
                                                                     <li><a href="/reference/onepage12">Onepage 12</a> </li>
                                                                     <li><a href="/reference/onepage13">Onepage 13</a> </li>
                                                                     <li><a href="/reference/onepage14">Onepage 14</a> </li>
                                                                     <li><a href="/reference/onepage15">Onepage 15</a> </li>
                                                                     <li><a href="/reference/onepage16">Onepage 16</a> </li>
                                                                     <li><a href="/reference/onepage17">Onepage 17</a> </li>
                                                                     <li><a href="#">Coming Soon</a> </li>
                                                                     <li class="last-item"><a href="#">Coming Soon</a> </li>
                                                                 </ul>
                                                             </div>
                                                            </div>
                                                        </li>
                                                    </ul> <!-- //.mega-menu --> 
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Pages</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/about">About Us</a></li>
                                                        <li class="menu-item-has-children">
                                                            <a href="#">Services</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/services">Services 1</a> </li>
                                                                <li class="last-item"><a href="/reference/services2">Services 2</a> </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="/reference/services-single">Services Single</a></li>
                                                        <li class="menu-item-has-children">
                                                            <a href="#">Our Peoples</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/team">Team Grid Style1</a> </li>
                                                                <li class="last-item"><a href="/reference/team2">Team Grid Style2</a> </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="/reference/price-tables">Price Tables</a></li>
                                                        <li class="last-item"><a href="/reference/faq">Faq</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Portfolios</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/portfolio">Portfolio One</a></li>
                                                        <li><a href="/reference/portfolio2">Portfolio Two</a></li>
                                                        <li><a href="/reference/portfolio3">Portfolio Three</a></li>
                                                        <li><a href="/reference/portfolio4">Portfolio Four</a></li>
                                                        <li><a href="/reference/portfolio5">Portfolio Five</a></li>
                                                        <li><a href="/reference/portfolio6">Portfolio Six</a></li>
                                                        <li class="last-item menu-item-has-children">
                                                            <a href="#">Portfolio Single</a>
                                                            <ul class="sub-menu">
                                                                <li><a href="/reference/portfolio-standard">Standard Style</a> </li>
                                                                <li><a href="/reference/portfolio-slider">Slider Style</a> </li>
                                                                <li class="last-item"><a href="/reference/portfolio-gallery">Gallery Style</a> </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Blog</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/blog">Blog</a></li>
                                                        <li class="last-item"><a href="/reference/blog-single">Blog Single</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Shop</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/shop">Shop</a></li>
                                                        <li><a href="/reference/shop-single">Shop Single</a></li>
                                                        <li><a href="/reference/cart">Cart</a></li>
                                                        <li><a href="/reference/checkout">Checkout</a></li>
                                                        <li class="last-item"><a href="/reference/account">My Account</a></li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item-has-children">
                                                    <a href="#">Contact</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="/reference/contact">Contact 1</a></li>
                                                        <li class="last-item"><a href="/reference/contact2">Contact 2</a></li>
                                                    </ul>
                                                </li>
                                            </ul> <!-- //.nav-menu -->
                                        </nav>
                                    </div> <!-- //.main-menu -->
                                    <div class="expand-btn-inner">
                                        <ul>
                                            <li class="search-parent">
                                                <a class="hidden-xs rs-search" data-bs-toggle="modal" data-bs-target="#searchModal" href="#">
                                                    <i class="flaticon-search"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a id="nav-expander" class="humburger nav-expander" href="#">
                                                    <span class="dot1"></span>
                                                    <span class="dot2"></span>
                                                    <span class="dot3"></span>
                                                    <span class="dot4"></span>
                                                    <span class="dot5"></span>
                                                    <span class="dot6"></span>
                                                    <span class="dot7"></span>
                                                    <span class="dot8"></span>
                                                    <span class="dot9"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu End -->

                <!-- Canvas Menu start -->
                <nav class="right_menu_togle hidden-md">
                    <div class="close-btn">
                        <span id="nav-close" class="humburger">
                            <span class="dot1"></span>
                            <span class="dot2"></span>
                            <span class="dot3"></span>
                            <span class="dot4"></span>
                            <span class="dot5"></span>
                            <span class="dot6"></span>
                            <span class="dot7"></span>
                            <span class="dot8"></span>
                            <span class="dot9"></span>
                        </span>
                    </div>
                    <div class="canvas-logo">
                        <a href="/reference/index"><img src="assets/images/logo-green2.png" alt="logo"></a>
                    </div>
                    <div class="offcanvas-text">
                        <p>Consectetur adipiscing elit. Duis at dictum risus, non suscip it arcu. Quisque aliquam posuere tortor aliquam posuere tortor develop database.</p>
                    </div>
                    <div class="canvas-contact">
                        <ul class="contact">
                            <li><i class="flaticon-location"></i> 374 William S Canning Blvd, Fall River MA 2721, USA</li>
                            <li><i class="flaticon-call"></i><a href="tel:+880155-69569">(+880)155-69569</a></li>
                            <li><i class="flaticon-email"></i><a href="mailto:support@rstheme.com">support@rstheme.com</a></li>
                            <li><i class="flaticon-clock"></i>10:00 - 17:00</li>
                        </ul>
                        <ul class="social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Canvas Menu end -->
            </header>
            <!--Header End-->
        </div>
        <!--Full width header End-->

		<!-- Main content Start -->
        <div class="main-content">
            <!-- Slider Start -->
            <div id="rs-slider" class="rs-slider slider6">
                <div class="slider-carousel owl-carousel">
                    <!-- Slide 1 -->
                    <div class="slider slide1">
                        <div class="container">
                            <div class="content-part">
                                <div class="slider-des">
                                    <div class="sl-subtitle mb-16">Expertly crafted policies</div>
                                    <h1 class="sl-title mb-18">Best insurance company in USA</h1>
                                    <div class="sl-desc">We are leading insurance providing company all over the world doing over 40 years.</div>
                                </div>
                                <div class="slider-bottom mt-40">
                                    <ul>
                                        <li><a href="/reference/contact" class="readon sl style6">Make a claim</a></li>
                                        <li><a href="/reference/contact" class="readon sl style6 active">Find an agent</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Slide 2 -->
                    <div class="slider slide2">
                        <div class="container">
                            <div class="content-part">
                                <div class="video-btn">
                                    <a class="popup-videos" href="https://www.youtube.com/watch?v=YLN1Argi7ik">
                                        <i class="fa fa-play"></i>
                                    </a>
                                </div>
                                <div class="slider-des">
                                    <h1 class="sl-title">Get covid-19 <span>support</span></h1>
                                </div>
                                <div class="slider-bottom">
                                    <ul>
                                        <li><a href="/reference/contact" class="readon sl style6">Make a claim</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slider End -->

            <!-- Services Section Start -->
            <div class="rs-services style7 pt-20 pb-90 lg-pb-85 md-pb-71 sm-pt-0">
                <div class="container">
                    <div class="sec-title style2 mb-60 md-mb-42">
                        <div class="first-half">
                            <div class="sub-title green">Best Services</div>
                            <h2 class="title mb-0">We ensure best insurance services for our clients.</h2>
                        </div>
                        <div class="last-half">
                            <p class="desc mb-0 pr-10">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old working with clients with better services.</p>
                        </div>
                    </div>
                    <div class="row gutter-16">
                        <div class="col-lg-3 col-sm-6 md-mb-16">
                            <div class="services-wrap">
                                <div class="icon-part">
                                    <img src="assets/images/services/style6/1.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="/reference/services-single">Home Insurance</a></h4>
                                    <div class="desc">Insurer can choose reobiz to get better insurance services with reliability</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 md-mb-16">
                            <div class="services-wrap">
                                <div class="icon-part">
                                    <img src="assets/images/services/style6/2.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="/reference/services-single">Car Insurance</a></h4>
                                    <div class="desc">Insurer can choose reobiz to get better insurance services with reliability</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 xs-mb-16">
                            <div class="services-wrap">
                                <div class="icon-part">
                                    <img src="assets/images/services/style6/3.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="/reference/services-single">Health Insurance</a></h4>
                                    <div class="desc">Insurer can choose reobiz to get better insurance services with reliability</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="services-wrap">
                                <div class="icon-part">
                                    <img src="assets/images/services/style6/4.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="/reference/services-single">Life Insurance</a></h4>
                                    <div class="desc">Insurer can choose reobiz to get better insurance services with reliability</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="more-btn mt-61 md-mt-40 text-center">
                        Looking more better services <a href="/reference/services-single">click here</a> for view all services.
                    </div>
                </div>
            </div>
            <!-- Services Section End -->

            <!-- About Section Start -->
            <div id="rs-about" class="rs-about style3 pb-100 md-pb-80">
                <div class="container">
                    <div class="row y-middle">
                        <div class="col-lg-6 pr-85 lg-pr-50 md-pr-15">
                            <div class="sec-title mb-24">
                                <div class="sub-title green">About Us</div>
                                <h2 class="title mb-21">We’re here to help you navigate insurance.</h2>
                                <div class="desc">Reobiz donec pulvinar magna id leoersi pellentesque impered dignissim rhoncus euismod euismod eros vitae.</div>
                            </div>
                            <ul class="listing-style mb-40">
                                <li>Production or trading of good or services for sale</li>
                                <li>Change in the volume of expected sales</li>
                                <li>Change in the volume of expected sales</li>
                            </ul>
                            <div class="btn-part">
                                <a class="readon" href="contact.html">Discover More</a>
                            </div>
                        </div>
                        <div class="col-lg-6 md-order-first md-mb-30">
                            <div class="row gutter-20">
                                <div class="col-6">
                                    <img src="assets/images/about/home6/1.jpg" alt="">
                                </div>
                                <div class="col-6 mt-30">
                                    <img class="mb-20" src="assets/images/about/home6/2.jpg" alt="">
                                    <img src="assets/images/about/home6/3.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Section End -->

            <!-- Why Choose Us Section Start -->
            <div class="rs-whychooseus style2 bg15 pt-92 pb-100 md-pt-72 md-pb-80">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-8">
                            <div class="sec-title mb-50 md-mb-43">
                                <div class="sub-title green">Why Choose Reobiz?</div>
                                <h2 class="title mb-21">We are award wining company!</h2>
                                <div class="desc">Reobiz donec pulvinar magna id leoersi pellentesque impered dignissim <br> rhoncus euismod euismod eros vitae.</div>
                            </div>
                            <div class="row gutter-16">
                                <div class="col-md-4 sm-mb-30">
                                    <div class="rs-counter-list plus">
                                        <div class="icon-part">
                                            <img src="assets/images/whychooseus/a.png" alt="">
                                        </div>
                                        <div class="counter-text">
                                            <div class="rs-count">500</div>
                                            <span class="title">Insurance Policies</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 sm-mb-30">
                                    <div class="rs-counter-list plus">
                                        <div class="icon-part">
                                            <img src="assets/images/whychooseus/b.png" alt="">
                                        </div>
                                        <div class="counter-text">
                                            <div class="rs-count">100</div>
                                            <span class="title">Awards WON</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="rs-counter-list thousand">
                                        <div class="icon-part">
                                            <img src="assets/images/whychooseus/c.png" alt="">
                                        </div>
                                        <div class="counter-text">
                                            <div class="rs-count">920</div>
                                            <span class="title">Happy Clients</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Why Choose Us Section End -->

            <!-- Free Quote Section Start -->
            <div class="rs-quote style1 pt-92 pb-100 md-pt-72 md-pb-80">
                <div class="container">
                    <div class="sec-title style2 mb-92 md-mb-42">
                        <div class="first-half">
                            <div class="sub-title green">Free Quote</div>
                            <h2 class="title mb-0">Get a free quote to create your desired insurance.</h2>
                        </div>
                        <div class="last-half">
                            <p class="desc mb-0 pr-10">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old working with clients with better services.</p>
                        </div>
                    </div>
                </div>
                <div class="row md-row-container">
                    <div class="col-lg-6 pr-40 md-pr-15 md-mb-30">
                        <div class="image-part text-right">
                            <img src="assets/images/quote/quote.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 custom">
                        <div id="form-messages"></div>
                        <form id="contact-form" class="quote-form" method="post" action="mailer.php">
                            <div class="form-ctrl mb-20">
                                <label>Enter Your Name</label>
                                <input type="text" name="name" placeholder="Enter Your Name" required>
                            </div>
                            <div class="form-ctrl mb-20">
                                <label>Enter Your Email</label>
                                <input type="email" name="email" placeholder="Enter Your Email" required>
                            </div>
                            <div class="form-ctrl mb-20">
                                <label>Type of Insurance</label>
                                <span class="select-option">
                                    <select required>
                                        <option value="">---</option>
                                        <option value="Health Insurance">Health Insurance</option>
                                        <option value="Car Insurance">Car Insurance</option>
                                        <option value="Home Insurance">Home Insurance</option>
                                        <option value="Business Insurance">Business Insurance</option>
                                        <option value="Life Insurance">Life Insurance</option>
                                        <option value="Travel Insurance">Travel Insurance</option>
                                    </select>
                                </span>
                            </div>
                            <div class="form-ctrl mb-20">
                                <label>Your Employment</label>
                                <input type="text" name="employment" placeholder="Your Employment" required>
                            </div>
                            <div class="form-ctrl mb-30">
                                <label>Annual Income</label>
                                <input type="text" name="income" placeholder="Annual Income" required>
                            </div>
                            <div class="submit-btn">
                                <button type="submit">Get Quote Now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Free Quote Section End -->

            <!-- Video Section Start -->
            <div class="rs-video style1 bg16 pt-200 pb-200 md-pt-100 md-pb-145 sm-pt-50 sm-pb-85">
                <div class="container">
                    <div class="content-part pb-30 pt-100">
                        <div class="video-btn border-style green mb-46">
                            <a class="popup-videos" href="https://www.youtube.com/watch?v=YLN1Argi7ik">
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                        <div class="video-desc">Watch awesome reviews from Reobiz insurance holders.
                        </div>
                    </div>
                </div>
            </div>
            <!-- Video Section End -->

            <!-- Testimonial Section Start -->
            <div class="rs-testimonial style4 bg17 pt-100 pb-86 md-pt-80 md-pb-71">
                <div class="container">
                    <div class="row y-middle">
                        <div class="col-lg-6 md-mb-30">
                            <img src="assets/images/testimonial/left-img4.jpg" alt="">
                        </div>
                        <div class="col-lg-6 pl-60 lg-pl-20 md-pl-15">
                            <div class="rs-carousel owl-carousel dot-style1" data-loop="true" data-items="1" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="1" data-lg-device="1" data-md-device-nav="false" data-md-device-dots="false">
                                <div class="content-wrap">
                                    <p class="desc">Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway.</p>
                                    <div class="testi-meta mt-49">
                                        <div class="detail-part">
                                            <h5 class="name">Delu Beta</h5>
                                            <div class="designation">CEO, Brick Consulting</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Testimonial Section End -->

            <!-- Partner Section Start -->
            <div class="rs-partner modify3 pt-70 pb-99 md-pb-79">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="4" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="4" data-md-device-nav="false" data-md-device-dots="false">
                    <div class="partner-item">
                        <a href="https://rstheme.com"><img src="assets/images/partner/gray/1.png" alt=""></a>
                    </div>
                    <div class="partner-item">
                        <a href="https://rstheme.com"><img src="assets/images/partner/gray/2.png" alt=""></a>
                    </div>
                    <div class="partner-item">
                        <a href="https://rstheme.com"><img src="assets/images/partner/gray/3.png" alt=""></a>
                    </div>
                    <div class="partner-item">
                        <a href="https://rstheme.com"><img src="assets/images/partner/gray/4.png" alt=""></a>
                    </div>
                    <div class="partner-item">
                        <a href="https://rstheme.com"><img src="assets/images/partner/gray/5.png" alt=""></a>
                    </div>
                </div>
            </div>
            <!-- Partner Section End -->

            <!-- Team Section Start -->
            <div class="rs-team grid1 bg18 green-bg pt-92 pb-100 md-pt-72 md-pb-75">
                <div class="container">
                    <div class="sec-title text-center mb-58 md-mb-41">
                        <div class="sub-title white-color">Expert People</div>
                        <h2 class="title mb-0 white-color">Get Consultancy From Our <br> Expert Agents</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-4 sm-mb-35">
                            <div class="team-item">
                                <div class="image-part">
                                    <a href="/reference/team-single"><img src="assets/images/team/1.jpg" alt=""></a>
                                </div>
                                <div class="content-part">
                                    <h4 class="name"><a href="/reference/team-single">Mike Jason</a></h4>
                                    <span class="designation">Business Advisor</span>
                                    <ul class="social-links">
                                        <li><a href="/reference/team-single"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 sm-mb-35">
                            <div class="team-item">
                                <div class="image-part">
                                    <a href="/reference/team-single"><img src="assets/images/team/2.jpg" alt=""></a>
                                </div>
                                <div class="content-part">
                                    <h4 class="name"><a href="/reference/team-single">Francis Ibikunle</a></h4>
                                    <span class="designation">Senior Consultant</span>
                                    <ul class="social-links">
                                        <li><a href="/reference/team-single"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="team-item">
                                <div class="image-part">
                                    <a href="/reference/team-single"><img src="assets/images/team/3.jpg" alt=""></a>
                                </div>
                                <div class="content-part">
                                    <h4 class="name"><a href="/reference/team-single">Ara Gates</a></h4>
                                    <span class="designation">Finance Consultant</span>
                                    <ul class="social-links">
                                        <li><a href="/reference/team-single"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="/reference/team-single"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Team Section End -->

            <!-- Blog Section Start -->
            <div id="rs-blog" class="rs-blog style1 modify2 green pt-100 pb-70 md-pt-72 md-pb-50">
                <div class="container">
                    <div class="sec-title style2 mb-92 md-mb-42">
                        <div class="first-half">
                            <div class="sub-title green">News</div>
                            <h2 class="title mb-0">Read latest news to get all new insurance policies</h2>
                        </div>
                        <div class="last-half">
                            <p class="desc mb-0 pr-10">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old working with clients with better services.</p>
                        </div>
                    </div>
                    <div class="rs-carousel owl-carousel dot-style1" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="3" data-lg-device="3" data-md-device-nav="false" data-md-device-dots="false">
                        <div class="blog-wrap">
                            <div class="img-part">
                                <img src="assets/images/blog/1.jpg" alt="">
                                <div class="fly-btn">
                                    <a href="/reference/blog-single"><i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                            <div class="content-part">
                                <a class="categories" href="blog-single.html">Strategy</a>
                                <h3 class="title"><a href="/reference/blog-single">Covid-19 threatens the next generation of smartphones</a></h3>
                                <p class="desc">We denounce with righteous indige nation and dislike men who are so beguiled and demo realized by...</p>
                                <div class="blog-meta">
                                    <div class="user-data">
                                        <img src="assets/images/blog/avatar/1.png" alt="">
                                        <span>admin</span>
                                    </div>
                                    <div class="date">
                                        <i class="fa fa-clock-o"></i> 06 Sep 2019
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-wrap">
                            <div class="img-part">
                                <img src="assets/images/blog/2.jpg" alt="">
                                <div class="fly-btn">
                                    <a href="/reference/blog-single"><i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                            <div class="content-part">
                                <a class="categories" href="blog-single.html">Management</a>
                                <h3 class="title"><a href="/reference/blog-single">Soundtrack filma Lady Exclusive Music</a></h3>
                                <p class="desc">We denounce with righteous indige nation and dislike men who are so beguiled and demo realized by...</p>
                                <div class="blog-meta">
                                    <div class="user-data">
                                        <img src="assets/images/blog/avatar/1.png" alt="">
                                        <span>admin</span>
                                    </div>
                                    <div class="date">
                                        <i class="fa fa-clock-o"></i> 06 Sep 2019
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-wrap">
                            <div class="img-part">
                                <img src="assets/images/blog/3.jpg" alt="">
                                <div class="fly-btn">
                                    <a href="/reference/blog-single"><i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                            <div class="content-part">
                                <a class="categories" href="blog-single.html">Consulting</a>
                                <h3 class="title"><a href="/reference/blog-single">Winged moved stars, fruit creature seed night.</a></h3>
                                <p class="desc">We denounce with righteous indige nation and dislike men who are so beguiled and demo realized by...</p>
                                <div class="blog-meta">
                                    <div class="user-data">
                                        <img src="assets/images/blog/avatar/1.png" alt="">
                                        <span>admin</span>
                                    </div>
                                    <div class="date">
                                        <i class="fa fa-clock-o"></i> 06 Sep 2019
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-wrap">
                            <div class="img-part">
                                <img src="assets/images/blog/4.jpg" alt="">
                                <div class="fly-btn">
                                    <a href="/reference/blog-single"><i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                            <div class="content-part">
                                <a class="categories" href="blog-single.html">Development</a>
                                <h3 class="title"><a href="/reference/blog-single">Given void great you’re good appear have i also fifth</a></h3>
                                <p class="desc">We denounce with righteous indige nation and dislike men who are so beguiled and demo realized by...</p>
                                <div class="blog-meta">
                                    <div class="user-data">
                                        <img src="assets/images/blog/avatar/1.png" alt="">
                                        <span>admin</span>
                                    </div>
                                    <div class="date">
                                        <i class="fa fa-clock-o"></i> 06 Sep 2019
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-wrap">
                            <div class="img-part">
                                <img src="assets/images/blog/5.jpg" alt="">
                                <div class="fly-btn">
                                    <a href="/reference/blog-single"><i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                            <div class="content-part">
                                <a class="categories" href="blog-single.html">Creative</a>
                                <h3 class="title"><a href="/reference/blog-single">Lights winged seasons fish abundantly evening.</a></h3>
                                <p class="desc">We denounce with righteous indige nation and dislike men who are so beguiled and demo realized by...</p>
                                <div class="blog-meta">
                                    <div class="user-data">
                                        <img src="assets/images/blog/avatar/1.png" alt="">
                                        <span>admin</span>
                                    </div>
                                    <div class="date">
                                        <i class="fa fa-clock-o"></i> 06 Sep 2019
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-wrap">
                            <div class="img-part">
                                <img src="assets/images/blog/6.jpg" alt="">
                                <div class="fly-btn">
                                    <a href="/reference/blog-single"><i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                            <div class="content-part">
                                <a class="categories" href="blog-single.html">Finance</a>
                                <h3 class="title"><a href="/reference/blog-single">Team You Want to Work With mistakes runners</a></h3>
                                <p class="desc">We denounce with righteous indige nation and dislike men who are so beguiled and demo realized by...</p>
                                <div class="blog-meta">
                                    <div class="user-data">
                                        <img src="assets/images/blog/avatar/1.png" alt="">
                                        <span>admin</span>
                                    </div>
                                    <div class="date">
                                        <i class="fa fa-clock-o"></i> 06 Sep 2019
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-wrap">
                            <div class="img-part">
                                <img src="assets/images/blog/3.jpg" alt="">
                                <div class="fly-btn">
                                    <a href="/reference/blog-single"><i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                            <div class="content-part">
                                <a class="categories" href="blog-single.html">Consulting</a>
                                <h3 class="title"><a href="/reference/blog-single">Winged moved stars, fruit creature seed night.</a></h3>
                                <p class="desc">We denounce with righteous indige nation and dislike men who are so beguiled and demo realized by...</p>
                                <div class="blog-meta">
                                    <div class="user-data">
                                        <img src="assets/images/blog/avatar/1.png" alt="">
                                        <span>admin</span>
                                    </div>
                                    <div class="date">
                                        <i class="fa fa-clock-o"></i> 06 Sep 2019
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Blog Section End -->
        </div> 
        <!-- Main content End -->

        <!-- Footer Start -->
        <footer id="rs-footer" class="rs-footer green-style">
            <div class="container">
                <div class="footer-newsletter">
                    <div class="row y-middle">
                        <div class="col-md-6 sm-mb-26">
                            <h3 class="title white-color mb-0">Newsletter Subscribe</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <form class="newsletter-form">
                                <input type="email" name="email" placeholder="Your email address" required="">
                                <button type="submit"><i class="fa fa-paper-plane"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer-content pt-62 pb-79 md-pb-64 sm-pt-48">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-widget md-mb-39">
                            <div class="about-widget pr-15">
                                <div class="logo-part">
                                    <a href="/reference/index"><img src="assets/images/logo-green.png" alt="Footer Logo"></a>
                                </div>
                                <p class="desc">We denounce with righteous indignation in and dislike men who are so beguiled and to demo realized by the charms of pleasure moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound.</p>
                                <div class="btn-part">
                                    <a class="readon" href="about.html">Discover More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 md-mb-32 footer-widget">
                            <h4 class="widget-title">Contact Info</h4>
                            <ul class="address-widget pr-40">
                                <li>
                                    <i class="flaticon-location"></i>
                                    <div class="desc">374 William S Canning Blvd, Fall River MA 2721, USA</div>
                                </li>
                                <li>
                                    <i class="flaticon-call"></i>
                                    <div class="desc">
                                        <a href="tel:+8801739753105">(+880)173-9753105</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-email"></i>
                                    <div class="desc">
                                        <a href="mailto:support@rstheme.com">support@rstheme.com</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-clock"></i>
                                    <div class="desc">
                                        10:00 - 17:00
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-widget">
                            <h4 class="widget-title">Latest Posts</h4>
                            <div class="footer-post">
                                <div class="post-wrap mb-15">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/1.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Covid-19 threatens the next generation of smartphones</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            September 6, 2019
                                        </div>
                                    </div>
                                </div>
                                <div class="post-wrap mb-15">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/2.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Soundtrack filma Lady Exclusive Music</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            April 15, 2019
                                        </div>
                                    </div>
                                </div>
                                <div class="post-wrap">
                                    <div class="post-img">
                                        <a href="/reference/blog-single"><img src="assets/images/blog/small/3.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="/reference/blog-single">Winged moved stars, fruit creature seed night.</a>
                                        <div class="date-post">
                                            <i class="fa fa-calendar"></i>
                                            October 9, 2019
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row y-middle">
                        <div class="col-lg-6 col-md-8 sm-mb-21">
                            <div class="copyright">
                                <p>© Copyright 2021 Reobiz. All Rights Reserved.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-4 text-right sm-text-center">
                            <ul class="footer-social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div id="scrollUp">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- End scrollUp  -->

        <!-- Search Modal Start -->
        <div class="modal fade search-modal" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
            <button type="button" class="close" data-bs-dismiss="modal">
                <span class="flaticon-cross"></span>
            </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here..." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- modernizr js -->
        <script src="assets/js/modernizr-2.8.3.min.js"></script>
        <!-- jquery latest version -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap v4.4.1 js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Menu js -->
        <script src="assets/js/rsmenu-main.js"></script> 
        <!-- op nav js -->
        <script src="assets/js/jquery.nav.js"></script>
        <!-- owl.carousel js -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Slick js -->
        <script src="assets/js/slick.min.js"></script>
        <!-- isotope.pkgd.min js -->
        <script src="assets/js/isotope.pkgd.min.js"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="assets/js/imagesloaded.pkgd.min.js"></script>
        <!-- wow js -->
        <script src="assets/js/wow.min.js"></script>
        <!-- aos js -->
        <script src="assets/js/aos.js"></script>
        <!-- Skill bar js -->
        <script src="assets/js/skill.bars.jquery.js"></script>
        <script src="assets/js/jquery.counterup.min.js"></script>         
         <!-- counter top js -->
        <script src="assets/js/waypoints.min.js"></script>
        <!-- video js -->
        <script src="assets/js/jquery.mb.YTPlayer.min.js"></script>
        <!-- magnific popup js -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Nivo slider js -->
        <script src="assets/inc/custom-slider/js/jquery.nivo.slider.js"></script>
        <!-- plugins js -->
        <script src="assets/js/plugins.js"></script>
        <!-- contact form js -->
        <script src="assets/js/contact.form.js"></script>
        <!-- ProgressBar Js -->
        <script src="assets/js/jQuery-plugin-progressbar.js"></script>
        <!-- main js -->
        <script src="assets/js/main.js"></script>
    </body>
</html>