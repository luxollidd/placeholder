<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('pages.home.index');})->name('home');
Route::get('/v2', function () {return view('pages.home.index-v2');})->name('home-v2');
Route::get('/product-1', function () {return view('pages.product-1.index');})->name('product-1');
Route::get('/mao', function(){return view('pages.test');})->name('test');

