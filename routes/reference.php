<?php

use Illuminate\Support\Facades\Route;
Route::prefix('reference')->group(function () {
Route::get('about', function () {return view('reference.about');});
Route::get('account', function () {return view('reference.account');});
Route::get('blog-single', function () {return view('reference.blog-single');});
Route::get('blog', function () {return view('reference.blog');});
Route::get('cart', function () {return view('reference.cart');});
Route::get('checkout', function () {return view('reference.checkout');});
Route::get('contact', function () {return view('reference.contact');});
Route::get('contact2', function () {return view('reference.contact2');});
Route::get('faq', function () {return view('reference.faq');});
Route::get('index', function () {return view('reference.index');});
Route::get('index10', function () {return view('reference.index10');});
Route::get('index11', function () {return view('reference.index11');});
Route::get('index12', function () {return view('reference.index12');});
Route::get('index13', function () {return view('reference.index13');});
Route::get('index14', function () {return view('reference.index14');});
Route::get('index15', function () {return view('reference.index15');});
Route::get('index16', function () {return view('reference.index16');});
Route::get('index17', function () {return view('reference.index17');});
Route::get('index18', function () {return view('reference.index18');});
Route::get('index2', function () {return view('reference.index2');});
Route::get('index3', function () {return view('reference.index3');});
Route::get('index4', function () {return view('reference.index4');});
Route::get('index5', function () {return view('reference.index5');});
Route::get('index6', function () {return view('reference.index6');});
Route::get('index7', function () {return view('reference.index7');});
Route::get('index8', function () {return view('reference.index8');});
Route::get('index9', function () {return view('reference.index9');});
Route::get('onepage1', function () {return view('reference.onepage1');});
Route::get('onepage10', function () {return view('reference.onepage10');});
Route::get('onepage11', function () {return view('reference.onepage11');});
Route::get('onepage12', function () {return view('reference.onepage12');});
Route::get('onepage13', function () {return view('reference.onepage13');});
Route::get('onepage14', function () {return view('reference.onepage14');});
Route::get('onepage15', function () {return view('reference.onepage15');});
Route::get('onepage16', function () {return view('reference.onepage16');});
Route::get('onepage17', function () {return view('reference.onepage17');});
Route::get('onepage2', function () {return view('reference.onepage2');});
Route::get('onepage3', function () {return view('reference.onepage3');});
Route::get('onepage4', function () {return view('reference.onepage4');});
Route::get('onepage5', function () {return view('reference.onepage5');});
Route::get('onepage6', function () {return view('reference.onepage6');});
Route::get('onepage8', function () {return view('reference.onepage8');});
Route::get('onepage9', function () {return view('reference.onepage9');});
Route::get('portfolio-gallery', function () {return view('reference.portfolio-gallery');});
Route::get('portfolio-slider', function () {return view('reference.portfolio-slider');});
Route::get('portfolio-standard', function () {return view('reference.portfolio-standard');});
Route::get('portfolio', function () {return view('reference.portfolio');});
Route::get('portfolio2', function () {return view('reference.portfolio2');});
Route::get('portfolio3', function () {return view('reference.portfolio3');});
Route::get('portfolio4', function () {return view('reference.portfolio4');});
Route::get('portfolio5', function () {return view('reference.portfolio5');});
Route::get('portfolio6', function () {return view('reference.portfolio6');});
Route::get('price-tables', function () {return view('reference.price-tables');});
Route::get('services-single', function () {return view('reference.services-single');});
Route::get('services', function () {return view('reference.services');});
Route::get('services2', function () {return view('reference.services2');});
Route::get('shop-single', function () {return view('reference.shop-single');});
Route::get('shop', function () {return view('reference.shop');});
Route::get('team-single', function () {return view('reference.team-single');});
Route::get('team', function () {return view('reference.team');});
Route::get('team2', function () {return view('reference.team2');});
Route::get('test.txt', function () {return view('reference.test.txt');});
});